from web import app

if __name__ == '__main__':
    print("Starting web server " + app.GetVersionString())
    app.run(debug=True)

import logging
from datetime import datetime
from pymongo import MongoClient, ReturnDocument

class DBClient:
    def __init__(self, testing = False, localhost = True):
        self.logger = logging.getLogger()

        self.ENVIRONMENT = "TEST" if testing else "DEV"
        server = "LOCAL" if localhost else "REMOTE"
        self.logger.info("DBClient settings: environment=%s; server=%s", self.ENVIRONMENT, server)

        if localhost:
            if testing:
                import mongomock
                self.mongoClient = mongomock.MongoClient()
            else:
                self.mongoClient = MongoClient()
        else:
            self.logger.error("ERROR: Remote server not implemented")
            # self.mongoClient = MongoClient("mongodb://littleredhen:JamesBond007@ds055680.mongolab.com:55680/bgscrape")

        if testing:
            self.DB_NAME = "htg_test"
        else:
            self.DB_NAME = "htg"

        db = self.mongoClient[self.DB_NAME]
        self.sequenceCountersCollection = db['sequence_counters']
        self.gamesCollection = db['games']
        self.gameLogsCollection = db['gameLogs']

        self._InitializeDatabase()

    def _InitializeDatabase(self):
        found = self.sequenceCountersCollection.find_one({'_id': "gameID"})
        if found is None:
            self.sequenceCountersCollection.insert_one({
                '_id': "gameID",
                'sequence_value': 0
            })

    def DestroyDatabase(self):
        if self.ENVIRONMENT == "TEST":
            self.mongoClient.drop_database(self.DB_NAME)
        else:
            self.logger.warning("Tried to destroy the database in a non-test environment!")

    def GetNextGameID(self):
        query = {'_id': "gameID"}
        update = {'$inc': {'sequence_value': 1}}

        result = self.sequenceCountersCollection.find_one_and_update(query, update, return_document=ReturnDocument.AFTER)
        return result['sequence_value']

    def GetGameByID(self, gameID):
        query = {"_id": gameID}

        result = self.gamesCollection.find_one(query)

        if result is None:
            return None

        result['id'] = result['_id']
        del result['_id']
        return result

    def GetAllGames(self):
        allGames = []

        for game in self.gamesCollection.find():
            game['id'] = game['_id']
            del game['_id']
            allGames.append(game)

        return allGames

    def InsertGame(self, game):
        game['_id'] = game['id']
        del game['id']
        return self.gamesCollection.insert_one(game)

    def UpdateGame(self, game):
        game['_id'] = game['id']
        del game['id']

        query = {'_id': game['_id']}

        return self.gamesCollection.find_one_and_replace(query, game)

    def InsertGameLog(self, gameID, message = ""):
        return self.gameLogsCollection.insert({"gameID": gameID, "message": message, "date": datetime.utcnow()})

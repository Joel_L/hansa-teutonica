class Actions:
    Unknown = 0
    Resupply = 1
    PlaceResource = 2
    DisplaceResource = 3.1
    PlaceDisplacedResource = 3.2
    MoveResources = 4
    EstablishTradeRoute = 5
    PlaceBonusMarkers = 6
    UseBonusMarker = 7

class Action:
    def __init__(self):
        self.name = "Unknown Action"
        self.type = Actions.Unknown

    def __str__(self):
        return self.name

    def GetName(self):
        return self.name

    def GetType(self):
        return self.type

class ResupplyAction(Action):
    def __init__(self, numTraders, numMerchants):
        self.name = "Re-supply"
        self.type = Actions.Resupply
        self.numTraders = numTraders
        self.numMerchants = numMerchants

class PlaceResourceAction(Action):
    def __init__(self, resourceType, targetFieldID):
        self.name = "Place Resource"
        self.type = Actions.PlaceResource
        self.resourceType = resourceType
        self.targetFieldID = targetFieldID

class DisplaceResourceAction(Action):
    def __init__(self, placementResourceType, numTradersPenalty, numMerchantsPenalty, targetFieldID):
        self.name = "Displace Resource"
        self.type = Actions.DisplaceResource
        self.placementResourceType = placementResourceType
        self.numTradersPenalty = numTradersPenalty
        self.numMerchantsPenalty = numMerchantsPenalty
        self.targetFieldID = targetFieldID

class PlaceDisplacedResourceReaction(Action):
    def __init__(self, resourceMovements):
        self.name = "Place Displaced Resource"
        self.type = Actions.PlaceDisplacedResource
        self.resourceMovements = resourceMovements

class DisplacedResourceData:
    def __init__(self, displacedResource, fieldID, numExtraResources):
        self.displacedResource = displacedResource
        self.fieldID = fieldID
        self.numExtraResources = numExtraResources

    def GetStateDict(self):
        return {
            'resource': self.displacedResource.GetStateDict(),
            'fieldID': self.fieldID,
            'numExtraResources': self.numExtraResources
        }

class DisplacedResourceMovement:
    def __init__(self, resourceType, targetFieldID, sourceFieldID = None):
        self.resourceType = resourceType
        self.targetFieldID = targetFieldID
        self.sourceFieldID = sourceFieldID

class MoveResourcesAction(Action):
    def __init__(self, sourceFieldIDs, targetFieldIDs):
        self.name = "Move Resources"
        self.type = Actions.MoveResources
        self.sourceFieldIDs = sourceFieldIDs
        self.targetFieldIDs = targetFieldIDs

class EstablishTradeRouteAction(Action):
    def __init__(self, routeFieldID, cityID, skillTypeToUpgrade = None):
        self.name = "Establish Trade Route"
        self.type = Actions.EstablishTradeRoute
        self.routeFieldID = routeFieldID
        self.cityID = cityID
        self.skillTypeToUpgrade = skillTypeToUpgrade

class PlaceBonusMarkersAction(Action):
    def __init__(self, targetFieldIDs):
        self.name = "Place Bonus Markers"
        self.type = Actions.PlaceBonusMarkers
        self.targetFieldIDs = targetFieldIDs

class UseBonusMarkerAction(Action):
    def __init__(self, bonusMarker):
        self.name = "Use Bonus Marker"
        self.type = Actions.UseBonusMarker
        self.bonusMarker = bonusMarker

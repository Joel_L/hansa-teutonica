def cycle_once_from_index(seq, startIndex):
    n = len(seq)
    for i in range(n):
        yield seq[(i + startIndex) % n]

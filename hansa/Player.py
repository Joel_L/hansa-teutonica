import logging
from .Enum import Enum
from .BonusMarker import BonusMarker
from .Skill import Skill, SkillTypes, TownKeySkill, ActionSkill, PrivilegeSkill, BookSkill, MoneyBagSkill
from .ResourcePool import ResourcePool
from .Resource import ResourceType

class PlayerColor:
    Red = Enum("red", 0)
    Green = Enum("green", 1)
    Blue = Enum("blue", 2)
    Yellow = Enum("yellow", 3)
    Purple = Enum("purple", 4)

    def GetAll(self):
        return [self.Red,
                self.Green,
                self.Blue,
                self.Yellow,
                self.Purple]

    def GetByName(self, name):
        for color in self.GetAll():
            if color.name == name:
                return color

        return None

class Player:
    def __init__(self, playerID, playerName, playerColor):
        self.logger = logging.getLogger()
        self.id = playerID
        self.name = playerName
        self.color = playerColor
        self.number = -1
        self.isStarting = False
        self.skills = []
        self.bonusMarkers = []
        self.numPoints = 0
        self.numActionsRemaining = 0
        self.stockResourcePool = None
        self.personalSupplyResourcePool = None

    def __str__(self):
        return self.name

    def GetStateDict(self):
        return {
            'id': self.id,
            'name': self.name,
            'color': self.color.ToDict(),
            'number': self.number,
            'isStarting': self.isStarting,
            'skills': [skill.GetStateDict() for skill in self.skills],
            'bonusMarkers': [bonusMarker.GetStateDict() for bonusMarker in self.bonusMarkers],
            'points': self.numPoints,
            'actionsRemaining': self.numActionsRemaining,
            'resources': {
                'stock': self.stockResourcePool.GetStateDict() if self.stockResourcePool is not None else None,
                'supply': self.personalSupplyResourcePool.GetStateDict() if self.personalSupplyResourcePool is not None else None
            }
        }

    @staticmethod
    def FromStateDict(stateDict):
        player = Player(stateDict['id'], stateDict['name'], None)
        player.color = PlayerColor().GetByName(stateDict['color']['name'])
        player.number = stateDict['number']
        player.isStarting = stateDict['isStarting']
        player.skills = [Skill.FromStateDict(skillDict) for skillDict in stateDict['skills']]
        player.bonusMarker = [BonusMarker.FromStateDict(bonusMarkerDict) for bonusMarkerDict in stateDict['bonusMarkers']]
        player.numPoints = stateDict['points']
        player.numActionsRemaining = stateDict['actionsRemaining']
        player.stockResourcePool = ResourcePool.FromStateDict(stateDict['resources']['stock']) if stateDict['resources']['stock'] is not None else None
        player.personalSupplyResourcePool = ResourcePool.FromStateDict(
            stateDict['resources']['supply']) if stateDict['resources']['supply'] is not None else None
        return player

    def Init(self, playerNum):
        self.number = playerNum
        self._InitPlayerBoard()
        self._InitPlayerScore()
        self._InitResourcePools()
        self._AssignStartingResources(playerNum)
        self.ResetActionCounter()

    def _InitPlayerBoard(self):
        self.logger.info("  Initializing player board")
        self.skills = [TownKeySkill.Copy(),
                       ActionSkill.Copy(),
                       PrivilegeSkill.Copy(),
                       BookSkill.Copy(),
                       MoneyBagSkill.Copy()]

    def _InitPlayerScore(self):
        self.logger.info("  Initializing player score")
        self.numPoints = 0

    def _InitResourcePools(self):
        self.logger.info("  Initializing resource pools")
        self.personalSupplyResourcePool = ResourcePool(0, 0)
        self.stockResourcePool = ResourcePool(11, 1)

    def _AssignStartingResources(self, playerNum):
        self.logger.info("  Assigning starting resources")
        self._MoveResourcesFromStockToSupply(5 + playerNum, 1)

    def _MoveResourcesFromStockToSupply(self, numTraders, numMerchants):
        actualNumTraders = min(numTraders, self.stockResourcePool.GetNumTraders())
        actualNumMerchants = min(numMerchants, self.stockResourcePool.GetNumMerchants())

        self.stockResourcePool.RemoveResources(actualNumTraders, actualNumMerchants)
        self.personalSupplyResourcePool.AddResources(numTraders, numMerchants)

        self.logger.info("Moved %s traders and %s merchants from stock to supply", actualNumTraders, actualNumMerchants)

    def GetID(self):
        return self.id

    def GetName(self):
        return self.name

    def GetColor(self):
        return self.color

    def GetNumber(self):
        return self.number

    def SetStartingPlayer(self, isStartingPlayer):
        self.isStarting = isStartingPlayer

    def _GetSkill(self, skillType):
        for skill in self.skills:
            if skill.GetType() == skillType:
                return skill

        return None

    def HasPrivilegeLevel(self, desiredPrivilegeLevel):
        return self._GetSkill(SkillTypes.Privilege).GetValue() >= desiredPrivilegeLevel

    def GetMovementAllowance(self):
        return self._GetSkill(SkillTypes.BookOfLore).GetValue()

    def GetPrivilegeLevel(self):
        return self._GetSkill(SkillTypes.Privilege).GetValue()

    def GetTownKeyValue(self):
        return self._GetSkill(SkillTypes.TownKey).GetValue()

    def UpgradeSkill(self, skillType):
        for skill in self.skills:
            if skill.GetType() == skillType:
                if skill.IsMastered():
                    message = "Tried to increase level beyond the maximum."
                    self.logger.error(message)
                    return False, message
                else:
                    valueBefore = skill.GetValue()
                    skill.IncreaseLevel()
                    valueDelta = skill.GetValue() - valueBefore
                    message = f"Upgraded skill '{skillType}' to level {skill.GetLevel()}."
                    self.logger.info(message)

                    # Special case: when the Actions skill is upgraded, the bonus action is usable immediately
                    if skillType == SkillTypes.Actions and valueDelta > 0:
                        self.AddActions(valueDelta)

                    self._AddResourceToSupply(skill.GetResourceType())
                    return True, message

        message = f"Trying to upgrade '{skillType}' skill, which doesn't exist."
        self.logger.exception(message)
        return False, message

    def GetNumMasteredSkills(self):
        numMasteredSkills = 0

        for skill in self.skills:
            if skill.IsMastered():
                numMasteredSkills += 1

        return numMasteredSkills

    def _AddResourceToSupply(self, resourceType):
        numTraders = 0
        numMerchants = 0

        if resourceType == ResourceType.Trader:
            numTraders = 1
            self.logger.info("Adding 1 trader to the supply.")
        elif resourceType == ResourceType.Merchant:
            numMerchants = 1
            self.logger.info("Adding 1 merchant to the supply.")
        else:
            raise ValueError("Asking for resource of type " + str(resourceType))

        self.personalSupplyResourcePool.AddResources(numTraders, numMerchants)

    def AddResourcesToStock(self, resources):
        for resource in resources:
            self.stockResourcePool.AddResource(resource.GetType())

    def ResetActionCounter(self):
        self.numActionsRemaining = self._GetSkill(SkillTypes.Actions).GetValue()

    def HasActions(self):
        return self.numActionsRemaining > 0

    def AddActions(self, numActionsToAdd):
        self.numActionsRemaining += numActionsToAdd
        self.logger.info("%s gained %s actions", self, numActionsToAdd)

    def SpendAction(self):
        self.numActionsRemaining -= 1

    def GetPoints(self):
        return self.numPoints

    def AddPoints(self, numPoints):
        self.numPoints += numPoints
        self.logger.info("%s gained %s prestige points", self, numPoints)

    def Resupply(self, numTraders, numMerchants):
        if numTraders + numMerchants > self._GetSkill(SkillTypes.Money).GetValue():
            message = "Tried to resupply more resources than allowed by money skill (wants {}, allowed {})".format(
                numTraders + numMerchants,
                self._GetSkill(SkillTypes.Money).GetValue())

            self.logger.error(message)
            return False, message

        if not self.HasResourceInStock(ResourceType.Trader, numTraders):
            message = "Not enough traders to move (wants {}, has {})".format(numTraders, self.stockResourcePool.GetNumTraders())
            self.logger.error(message)
            return False, message

        if not self.HasResourceInStock(ResourceType.Merchant, numMerchants):
            message = "Not enough merchants to move (wants {}, has {})".format(numMerchants, self.stockResourcePool.GetNumMerchants())
            self.logger.error(message)
            return False, message

        self._MoveResourcesFromStockToSupply(numTraders, numMerchants)
        return True, ""

    def HasResourceInStock(self, resourceType, count = 1):
        if resourceType == ResourceType.Trader:
            return self.stockResourcePool.GetNumTraders() >= count
        elif resourceType == ResourceType.Merchant:
            return self.stockResourcePool.GetNumMerchants() >= count

        raise ValueError("Asking for resource of type " + str(resourceType))

    def IsStockEmpty(self):
        return self.stockResourcePool.GetNumTraders() == 0 and self.stockResourcePool.GetNumMerchants() == 0

    def RemoveResourceFromStock(self, resourceType):
        self.stockResourcePool.RemoveResource(resourceType, 1)
        self.logger.info("Removed a %s from stock", resourceType)

    def HasResourceInSupply(self, resourceType, count = 1):
        if resourceType == ResourceType.Trader:
            return self.personalSupplyResourcePool.GetNumTraders() >= count
        elif resourceType == ResourceType.Merchant:
            return self.personalSupplyResourcePool.GetNumMerchants() >= count

        raise ValueError("Asking for resource of type " + str(resourceType))

    def HasResourcesInSupply(self, numTraders, numMerchants):
        return self.HasResourceInSupply(ResourceType.Trader, numTraders) and self.HasResourceInSupply(ResourceType.Merchant, numMerchants)

    def IsSupplyEmpty(self):
        return self.personalSupplyResourcePool.GetNumTraders() == 0 and self.personalSupplyResourcePool.GetNumMerchants() == 0

    def RemoveResourceFromSupply(self, resourceType):
        self.personalSupplyResourcePool.RemoveResource(resourceType, 1)

    def MoveResourcesFromSupplyToStock(self, numTraders, numMerchants):
        self.personalSupplyResourcePool.RemoveResources(numTraders, numMerchants)
        self.stockResourcePool.AddResources(numTraders, numMerchants)

        self.logger.info("Moved %s traders and %s merchants from supply to stock", numTraders, numMerchants)

    def TakeBonusMarker(self, bonusMarker):
        self.bonusMarkers.append(bonusMarker)
        self.logger.info("Took bonus marker %s", bonusMarker)

    def GetNumBonusMarkers(self):
        return len(self.bonusMarkers)

    def GetUnusedBonusMarkerOfClassType(self, bonusMarkerClassTypeToFind):
        for bonusMarker in self.bonusMarkers:
            if isinstance(bonusMarker, type(bonusMarkerClassTypeToFind)) and bonusMarker.IsActive():
                return bonusMarker

        return None

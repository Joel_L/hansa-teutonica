class Enum:
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __str__(self):
        return self.name

    def ToDict(self):
        return self.__dict__

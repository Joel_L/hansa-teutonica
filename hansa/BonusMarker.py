from .Enum import Enum

class BonusType:
    RemoveResources = Enum("Remove Resources", 0)
    AdditionalOffice = Enum("Additional Office", 1)
    SwapOffices = Enum("Swap Offices", 2)
    ExtraActions = Enum("Extra Actions", 3)
    IncreaseSkill = Enum("Increase Skill", 4)

    def GetAll(self):
        return [self.RemoveResources,
                self.AdditionalOffice,
                self.SwapOffices,
                self.ExtraActions,
                self.IncreaseSkill]

    def GetByValue(self, value):
        for resourceType in self.GetAll():
            if resourceType.value == value:
                return resourceType

class BonusMarker(object):
    def __init__(self, bonusType):
        self.type = bonusType
        self.active = True

    def __str__(self):
        return str(self.type)

    def GetStateDict(self):
        return {
            'type': self.type.ToDict(),
            'isActive': self.IsActive()
        }

    @staticmethod
    def FromStateDict(stateDict):
        bonusMarker = BonusMarker(None)
        bonusMarker.type = BonusType().GetByValue(stateDict['type']['value'])
        bonusMarker.active = stateDict['isActive']
        return bonusMarker

    def GetType(self):
        return self.type

    def IsActive(self):
        return self.active

    def PrepareForUse(self):
        pass

    def Use(self):
        self.active = False

class RemoveResourcesBonusMarker(BonusMarker):
    def __init__(self, numResources):
        super(RemoveResourcesBonusMarker, self).__init__(BonusType.RemoveResources)
        self.numResources = numResources
        self.targetFieldIDs = []

    def PrepareForUse(self, targetFieldIDs):
        super(RemoveResourcesBonusMarker, self).PrepareForUse()
        self.targetFieldIDs = targetFieldIDs

class RemoveResources3BonusMarker(RemoveResourcesBonusMarker):
    def __init__(self):
        super(RemoveResources3BonusMarker, self).__init__(3)

class AdditionalOfficeBonusMarker(BonusMarker):
    def __init__(self):
        super(AdditionalOfficeBonusMarker, self).__init__(BonusType.AdditionalOffice)
        self.cityID = -1

    def PrepareForUse(self, cityID):
        super(AdditionalOfficeBonusMarker, self).PrepareForUse()
        self.cityID = cityID

class SwapOfficesBonusMarker(BonusMarker):
    def __init__(self):
        super(SwapOfficesBonusMarker, self).__init__(BonusType.SwapOffices)
        self.cityID = -1
        self.officeIndex = -1

    def PrepareForUse(self, cityID, firstOfficeIndex):
        super(SwapOfficesBonusMarker, self).PrepareForUse()
        self.cityID = cityID
        self.firstOfficeIndex = firstOfficeIndex

class ExtraActionsBonusMarker(BonusMarker):
    def __init__(self, numActions):
        super(ExtraActionsBonusMarker, self).__init__(BonusType.ExtraActions)
        self.numActions = numActions

class ExtraActions3BonusMarker(ExtraActionsBonusMarker):
    def __init__(self):
        super(ExtraActions3BonusMarker, self).__init__(3)

class ExtraActions4BonusMarker(ExtraActionsBonusMarker):
    def __init__(self):
        super(ExtraActions4BonusMarker, self).__init__(4)

class IncreaseSkillBonusMarker(BonusMarker):
    def __init__(self):
        super(IncreaseSkillBonusMarker, self).__init__(BonusType.IncreaseSkill)
        self.skillType = None

    def PrepareForUse(self, skillType):
        super(IncreaseSkillBonusMarker, self).PrepareForUse()
        self.skillType = skillType

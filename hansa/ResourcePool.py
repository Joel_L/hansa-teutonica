from .Resource import ResourceType

class ResourcePool:
    def __init__(self, numTraders, numMerchants):
        self.numTraders = numTraders
        self.numMerchants = numMerchants

    def GetStateDict(self):
        return {
            'traders': self.numTraders,
            'merchants': self.numMerchants
        }

    @staticmethod
    def FromStateDict(stateDict):
        return ResourcePool(stateDict['traders'], stateDict['merchants'])

    def GetNumTraders(self):
        return self.numTraders

    def GetNumMerchants(self):
        return self.numMerchants

    def AddResource(self, resourceType):
        if resourceType == ResourceType.Trader:
            self.numTraders += 1
        elif resourceType == ResourceType.Merchant:
            self.numMerchants += 1
        else:
            raise ValueError("Trying to add invalid resource type " + str(resourceType))

    def AddResources(self, numTradersToAdd = 0, numMerchantsToAdd = 0):
        if numTradersToAdd < 0 or numMerchantsToAdd < 0:
            raise ValueError("Trying to add a negative amount of resources")

        self.numTraders += numTradersToAdd
        self.numMerchants += numMerchantsToAdd

    def RemoveResources(self, numTradersToRemove = 0, numMerchantsToRemove = 0):
        self.RemoveResource(ResourceType.Trader, numTradersToRemove)
        self.RemoveResource(ResourceType.Merchant, numMerchantsToRemove)

    def RemoveResource(self, resourceType, amountToRemove):
        if amountToRemove < 0:
            raise ValueError("Trying to remove a negative amount of resources")

        if resourceType == ResourceType.Trader:
            self.numTraders -= amountToRemove
            assert self.numTraders >= 0, "Removed more traders than the player has."

        elif resourceType == ResourceType.Merchant:
            self.numMerchants -= amountToRemove
            assert self.numMerchants >= 0, "Removed more merchants than the player has."

        else:
            raise ValueError("Trying to remove invalid resource type " + str(resourceType))

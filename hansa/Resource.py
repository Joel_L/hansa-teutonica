from .Enum import Enum

class ResourceType:
    Vacant = Enum("vacant", 0)
    Trader = Enum("trader", 1)
    Merchant = Enum("merchant", 2)

    def GetAll(self):
        return [self.Vacant,
                self.Trader,
                self.Merchant]

    def GetByName(self, name):
        for resourceType in self.GetAll():
            if resourceType.name == name:
                return resourceType

        return None

class Resource:
    def __init__(self, resourceType, ownerPlayerID = None):
        self.type = resourceType
        self.ownerPlayerID = ownerPlayerID

    def __str__(self):
        if self.ownerPlayerID is None:
            return "no resource"
        else:
            return f"{self.ownerPlayerID}'s {self.type}"

    def GetStateDict(self):
        return {
            "type": str(self.type),
            "ownerID": self.ownerPlayerID if self.type is not ResourceType.Vacant else ""
        }

    @staticmethod
    def FromStateDict(stateDict):
        resource = Resource(ResourceType().GetByName(stateDict['type']), stateDict['ownerID'])
        return resource

    def GetType(self):
        return self.type

    def GetOwnerPlayerID(self):
        return self.ownerPlayerID

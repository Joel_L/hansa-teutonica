import logging
from .Office import Office
from .Skill import SkillTypes

class City:
    def __init__(self, cityID, name, offices, skillTypes = []):
        self.logger = logging.getLogger()
        self.id = cityID
        self.name = name
        self.offices = offices
        self.skillTypes = skillTypes

    def __str__(self):
        return self.name

    def GetStateDict(self, playerID = None):
        return {
            'id': self.id,
            'name': self.name,
            'offices': [office.GetStateDict(playerID) for office in self.offices],
            'skillTypes': [skillType.ToDict() for skillType in self.skillTypes]
        }

    @staticmethod
    def FromStateDict(stateDict):
        city = City(stateDict['id'], stateDict['name'], None)
        city.offices = [Office.FromStateDict(officeState) for officeState in stateDict['offices']]
        city.skillTypes = [SkillTypes().GetByValue(skillType['value']) for skillType in stateDict['skillTypes']]
        return city

    def Output(self):
        skillString = ""
        if len(self.skillTypes) > 0:
            skillString = " - {}".format(", ".join(str(skillType) for skillType in self.skillTypes))

        officeString = " ".join([office.Output() for office in self.offices])

        return "[{} ({}{}): {}]".format(self.name, self.id, skillString, officeString)

    def GetID(self):
        return self.id

    def HasSkillType(self, skillType):
        return skillType in self.skillTypes

    def GetOfficeByIndex(self, officeIndex):
        if officeIndex >= 0 and officeIndex < len(self.offices):
            return self.offices[officeIndex]

        self.logger.error("No office at index %s", officeIndex)
        return None

    def GetOccupiedOffices(self):
        occupiedOffices = []

        for office in self.offices:
            if office.IsOccupied():
                occupiedOffices.append(office)

        return occupiedOffices

    def GetLeftmostVacantOffice(self):
        for office in self.offices:
            if office.IsVacant():
                return office

        return None

    def IsFull(self):
        return self.GetLeftmostVacantOffice() is None

    def GetControllingPlayerID(self):
        playerOfficeCounts, highestNumOffices = self._GetPlayerOfficeCounts()
        mostOfficePlayerIDs = []

        for playerID, numOffices in playerOfficeCounts.items():
            if numOffices == highestNumOffices:
                mostOfficePlayerIDs.append(playerID)

        controllingPlayerID = None

        if len(mostOfficePlayerIDs) > 1:
            controllingPlayerID = self._GetRightmostOccupiedOffice().GetOccupyingPlayerID()
        elif len(mostOfficePlayerIDs) == 1:
            controllingPlayerID = mostOfficePlayerIDs[0]

        return controllingPlayerID

    def HasOfficeOccupiedByPlayer(self, player):
        for office in self.GetOccupiedOffices():
            if office.GetOccupyingPlayerID() == player.GetID():
                return True

        return False

    def _GetPlayerOfficeCounts(self):
        playerOfficeCounts = {}
        highestNumOffices = 0
        for office in self.offices:
            occupyingPlayerID = office.GetOccupyingPlayerID()
            if occupyingPlayerID is not None:
                if occupyingPlayerID in playerOfficeCounts:
                    playerOfficeCounts[occupyingPlayerID] += 1
                else:
                    playerOfficeCounts[occupyingPlayerID] = 1

                if playerOfficeCounts[occupyingPlayerID] > highestNumOffices:
                    highestNumOffices = playerOfficeCounts[occupyingPlayerID]

        return playerOfficeCounts, highestNumOffices

    def GetOfficeCountForPlayer(self, player):
        playerOfficeCounts = self._GetPlayerOfficeCounts()

        if player.GetID() in playerOfficeCounts:
            return playerOfficeCounts[player.GetID()]

        return 0

    def _GetRightmostOccupiedOffice(self):
        for office in reversed(self.offices):
            if office.IsOccupied():
                return office

        return None

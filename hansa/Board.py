import logging
from .City import City
from .Route import Route
from .Field import Field

class Board:
    def __init__(self):
        self.logger = logging.getLogger()
        self.cities = []
        self.routes = []
        self.startingRoutes = [] # for initial bonus marker placement

    def GetStateDict(self, playerID = None):
        if playerID is not None:
            return {
                'offices': [office.GetStateDict(playerID) for office in self._GetAllOccupiedOffices()],
                'fields': [field.GetStateDict() for field in self._GetAllOccupiedFields()],
                'bonusMarkers': [{
                    'route': route.GetID(),
                    'bonusMarker': route.GetBonusMarker().GetStateDict()
                } for route in self._GetAllRoutesWithBonusMarkers()]
            }
        else:
            return {
                'cities': [city.GetStateDict(playerID) for city in self.cities],
                'routes': [route.GetStateDict() for route in self.routes],
                'startingRoutes': [route.GetStateDict() for route in self.startingRoutes]
            }

    @staticmethod
    def FromStateDict(stateDict):
        board = Board()
        board.cities = [City.FromStateDict(cityState) for cityState in stateDict['cities']]
        board.routes = [Route.FromStateDict(routeState) for routeState in stateDict['routes']]
        board.startingRoutes = [Route.FromStateDict(routeState) for routeState in stateDict['startingRoutes']]
        return board

    def Output(self):
        routesAsStrings = []

        for route in self.routes:
            routesAsStrings.append(route.Output())

        return ' '.join(routesAsStrings)

    def GetCityByID(self, cityID):
        for city in self.cities:
            if city.GetID() == cityID:
                return city

        return None

    def AddCity(self, city):
        self.cities.append(city)

    def AddRoute(self, route):
        self.routes.append(route)

    def AddStartingRoute(self, route):
        self.startingRoutes.append(route)

    def _GetAllRoutesWithBonusMarkers(self):
        routesWithBonusMarkers = []
        for route in self.routes:
            if route.HasBonusMarker():
                routesWithBonusMarkers.append(route)

        return routesWithBonusMarkers

    def _GetAllOccupiedOffices(self):
        occupiedOffices = []
        for city in self.cities:
            for office in city.GetOccupiedOffices():
                occupiedOffices.append(office)

        return occupiedOffices

    def _GetAllOccupiedFields(self):
        occupiedFields = []
        for route in self.routes:
            for field in route.GetFields():
                if field.IsOccupied():
                    occupiedFields.append(field)

        return occupiedFields

    def GetFieldByID(self, fieldID):
        for route in self.routes:
            for field in route.GetFields():
                if field.GetID() == fieldID:
                    return field

        self.logger.error("Field ID %s not found", fieldID)
        return Field(-1)

    def AddResourceToField(self, resource, fieldID):
        field = self.GetFieldByID(fieldID)
        if field.GetID() != fieldID:
            return False
        elif field.IsOccupied():
            self.logger.error("Field ID %s is occupied.", fieldID)
            return False
        else:
            self.logger.info("%s added to field ID %s", resource, fieldID)
            field.Occupy(resource)
            return True

    def ReplaceResourceInField(self, resource, fieldID):
        field = self.GetFieldByID(fieldID)
        if field.GetID() != fieldID:
            return False, None
        elif not field.IsOccupied():
            self.logger.error("There is no resource occupying field ID %s", fieldID)
            return False, None
        elif field.GetOccupyingResource().GetOwnerPlayerID() == resource.GetOwnerPlayerID():
            self.logger.error("Player %s already has a resource occupying field ID %s", resource.GetOwnerPlayerID(), fieldID)
            return False, None
        else:
            replacedResource = field.Vacate()
            field.Occupy(resource)
            self.logger.info("Replaced %s occupying field ID %s with %s", replacedResource, fieldID, resource)
            return True, replacedResource

    def MovePlayerResources(self, player, sourceFieldIDs, targetFieldIDs):
        if (len(sourceFieldIDs) != len(targetFieldIDs)):
            self.logger.error("Number of source fields is different than destination")
            return False

        sourceFields = []
        for fieldID in sourceFieldIDs:
            field = self.GetFieldByID(fieldID)
            if field.GetID() != fieldID:
                return False
            elif not field.IsOccupiedBy(player):
                self.logger.error("%s does not have a resource occupying field ID %s", player, fieldID)
                return False

            sourceFields.append(field)

        targetFields = []
        for fieldID in targetFieldIDs:
            field = self.GetFieldByID(fieldID)
            if field.GetID() != fieldID:
                return False
            elif field.IsOccupied():
                self.logger.error("Field ID %s is already occupied by %s", fieldID, field.GetOccupyingResource())
                return False

            targetFields.append(field)

        for i, sourceField in enumerate(sourceFields):
            movingResource = sourceField.Vacate()
            targetFields[i].Occupy(movingResource)

        resourceText = "a resource" if len(sourceFieldIDs) == 1 else "resources"
        idText = "ID" if len(sourceFieldIDs) == 1 else "IDs"

        self.logger.info("Moved %s from field %s %s to field %s %s",
                         resourceText,
                         idText, ",".join([str(fieldID) for fieldID in sourceFieldIDs]),
                         idText, ",".join(str(fieldID) for fieldID in targetFieldIDs))
        return True

    def RemoveResources(self, fieldIDs):
        fields = []
        for fieldID in fieldIDs:
            field = self.GetFieldByID(fieldID)
            if field.GetID() != fieldID:
                message = "Can't remove resource from invalid field ID {}".format(fieldID)
                self.logger.error(message)
                return False, message

            if not field.IsOccupied():
                message = "Can't remove resource from empty field ID {}".format(fieldID)
                self.logger.error(message)
                return False, message

            fields.append(field)

        for field in fields:
            field.Vacate()

        message = "Removed resources from field IDs {}".format(", ".join(fieldID for fieldID in fieldIDs))
        self.logger.info(message)
        return True, message

    def GetRouteByFieldID(self, fieldID):
        for route in self.routes:
            for field in route.GetFields():
                if field.GetID() == fieldID:
                    return route

        self.logger.error("Field ID %s is not on any route", fieldID)
        return Route(-1, [], [])

    def _GetAdjacentRoutes(self, startRoute):
        adjacentRoutes = []
        for route in self.routes:
            if route != startRoute:
                for city in route.GetCities():
                    if city in startRoute.GetCities():
                        adjacentRoutes.append(route)

        return adjacentRoutes

    def _FindNearestVacantRoutesToRoute(self, route, visitedRoutes = None):
        if visitedRoutes is None:
            visitedRoutes = set()

        vacantRoutes = []
        visitedRoutes.add(route)

        adjacentRoutes = self._GetAdjacentRoutes(route)
        for adjacentRoute in adjacentRoutes:
            if not adjacentRoute.IsFull():
                vacantRoutes.append(adjacentRoute)

        if len(vacantRoutes) <= 0:
            for adjacentRoute in adjacentRoutes:
                if adjacentRoute not in visitedRoutes:
                    vacantRoutes.extend(self._FindNearestVacantRoutesToRoute(adjacentRoute, visitedRoutes))

        return vacantRoutes

    def _FindNearestVacantRoutesToFieldID(self, fieldID):
        return self._FindNearestVacantRoutesToRoute(self.GetRouteByFieldID(fieldID))

    def IsFieldOnNearestVacantRouteFromStartField(self, startFieldID, otherFieldID):
        vacantRoutes = self._FindNearestVacantRoutesToFieldID(startFieldID)

        for vacantRoute in vacantRoutes:
            if vacantRoute.ContainsFieldID(otherFieldID):
                return True

        return False

    def AddBonusMarkersToStartingRoutes(self, bonusMarkers):
        for route, bonusMarker in zip(self.startingRoutes, bonusMarkers):
            route.AddBonusMarker(bonusMarker)

        self.logger.info("Added %s bonus markers to the starting routes", len(bonusMarkers))

    def SwapAdjacentOfficesInCity(self, cityID, firstOfficeIndex):
        city = self.GetCityByID(cityID)

        if city is not None:
            office1 = city.GetOfficeByIndex(firstOfficeIndex)
            office2 = city.GetOfficeByIndex(firstOfficeIndex + 1)

            if office1 is not None and office2 is not None:
                if office1.IsOccupied():
                    if office2.IsOccupied():
                        resource1 = office1.Vacate()
                        resource2 = office2.Vacate()
                        office1.Occupy(resource2)
                        office2.Occupy(resource1)
                        self.logger.info("Swapped %s with %s", resource1, resource2)
                        return True
                    else:
                        self.logger.error("Office at index %s is vacant", firstOfficeIndex + 1)
                else:
                    self.logger.error("Office at index %s is vacant", firstOfficeIndex)

        return False

    def GetEndOfGameOfficePointsByPlayer(self):
        pointsByPlayer = {}

        for city in self.cities:
            for office in city.GetOccupiedOffices():
                if office.HasPointsAwardedAtEndOfGame():
                    playerID = office.GetOccupyingPlayerID()
                    if playerID in pointsByPlayer:
                        pointsByPlayer[playerID] += office.GetNumPointsToAwardOccupyingPlayer()
                    else:
                        pointsByPlayer[playerID] = office.GetNumPointsToAwardOccupyingPlayer()

        return pointsByPlayer

    def GetNumControlledCitiesByPlayer(self):
        numControlledCitiesByPlayer = {}

        for city in self.cities:
            controllingPlayerID = city.GetControllingPlayerID()

            if controllingPlayerID is not None:
                if controllingPlayerID in numControlledCitiesByPlayer:
                    numControlledCitiesByPlayer[controllingPlayerID] += 1
                else:
                    numControlledCitiesByPlayer[controllingPlayerID] = 1

        return numControlledCitiesByPlayer

    def _GetAdjacentCities(self, startCity):
        adjacentCities = []

        for route in self.routes:
            (city1, city2) = route.GetCities()
            if city1 == startCity and city2 not in adjacentCities:
                adjacentCities.append(city2)
            elif city2 == startCity and city1 not in adjacentCities:
                adjacentCities.append(city1)

        return adjacentCities

    def _GetNetworkFromCityForPlayer(self, player, startCity, citiesInNetwork = None):
        if citiesInNetwork is None:
            citiesInNetwork = set()

        if startCity.HasOfficeOccupiedByPlayer(player) and startCity not in citiesInNetwork:
            citiesInNetwork.add(startCity)

            for city in self._GetAdjacentCities(startCity):
                self._GetNetworkFromCityForPlayer(player, city, citiesInNetwork)

        return citiesInNetwork

    def _GetLargestNetworkForPlayer(self, player):
        networkSize = 0
        largestNetworkSize = 0
        citiesInNetwork = []
        citiesInLargestNetwork = []

        for city in self.cities:
            citiesInNetwork = self._GetNetworkFromCityForPlayer(player, city)
            networkSize = len(citiesInNetwork)

            if networkSize > largestNetworkSize:
                largestNetworkSize = networkSize
                citiesInLargestNetwork = citiesInNetwork

        return citiesInLargestNetwork

    def GetNumOfficesInLargestNetworkForPlayer(self, player):
        numOffices = 0

        for city in self._GetLargestNetworkForPlayer(player):
            numOffices += city.GetOfficeCountForPlayer(player)

        return numOffices

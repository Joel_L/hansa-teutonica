import logging
from copy import copy
from .Resource import Resource, ResourceType

class Office:
    def __init__(self, officeID, resourceType, privilegeLevel, numPointsToAwardOccupyingPlayer = 0, arePointsAwardedAtEndOfGame = False):
        self.logger = logging.getLogger()
        self.id = officeID
        self.resourceType = resourceType
        self.privilegeLevel = privilegeLevel
        self.occupyingResource = Resource(ResourceType.Vacant)
        self.numPointsToAwardOccupyingPlayer = numPointsToAwardOccupyingPlayer
        self.arePointsAwardedAtEndOfGame = arePointsAwardedAtEndOfGame

    def GetStateDict(self, playerID = None):
        if playerID is not None:
            return self.occupyingResource.GetStateDict()
        else:
            return {
                'id': self.id,
                'resourceType': self.resourceType.ToDict(),
                'privilegeLevel': self.privilegeLevel,
                'occupyingResource': self.occupyingResource.GetStateDict(),
                'numPointsToAwardOccupyingPlayer': self.numPointsToAwardOccupyingPlayer,
                'arePointsAwardedAtEndOfGame': self.arePointsAwardedAtEndOfGame
            }

    @staticmethod
    def FromStateDict(stateDict):
        office = Office(
            stateDict['id'],
            ResourceType().GetByName(stateDict['resourceType']['name']),
            stateDict['privilegeLevel'],
            stateDict['numPointsToAwardOccupyingPlayer'],
            stateDict['arePointsAwardedAtEndOfGame'])
        return office

    def Output(self):
        text = str(self.resourceType)[0] + str(self.privilegeLevel)

        if self.occupyingResource.GetType() != ResourceType.Vacant:
            text += f"({self.occupyingResource})"

        return text

    def GetID(self):
        return self.id

    def GetResourceType(self):
        return self.resourceType

    def GetPrivilegeLevel(self):
        return self.privilegeLevel

    def GetOccupyingPlayerID(self):
        return self.occupyingResource.GetOwnerPlayerID()

    def IsVacant(self):
        return self.occupyingResource.GetOwnerPlayerID() is None

    def IsOccupied(self):
        return not self.IsVacant()

    def Occupy(self, resource):
        self.occupyingResource = resource

    def Vacate(self):
        vacatingResource = copy(self.occupyingResource)
        self.occupyingResource = Resource(ResourceType.Vacant)
        return vacatingResource

    def GetNumPointsToAwardOccupyingPlayer(self):
        return self.numPointsToAwardOccupyingPlayer

    def HasPointsAwardedAtEndOfGame(self):
        return self.arePointsAwardedAtEndOfGame

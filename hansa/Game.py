import logging
import random
from .Enum import Enum
from .Util import cycle_once_from_index
from .Board import Board
from .BoardImporter import BoardImporter
from .Player import Player, PlayerColor
from .Action import Actions, DisplacedResourceData
from .Resource import Resource, ResourceType
from .BonusMarker import BonusMarker, BonusType
from .BonusMarker import AdditionalOfficeBonusMarker, SwapOfficesBonusMarker, RemoveResources3BonusMarker
from .BonusMarker import ExtraActions3BonusMarker, ExtraActions4BonusMarker, IncreaseSkillBonusMarker

class GameStatus:
    WaitingForMorePlayers = Enum("waiting for more players", 0)
    ReadyToStart = Enum("ready to start", 1)
    WaitingForCurrentPlayer = Enum("waiting for the current player", 2)
    WaitingForPlayerReaction = Enum("waiting for another player to react", 3)
    GameOver = Enum("game is over", 4)

    def GetAll(self):
        return [self.WaitingForMorePlayers,
                self.ReadyToStart,
                self.WaitingForCurrentPlayer,
                self.WaitingForPlayerReaction,
                self.GameOver]

    def GetByValue(self, value):
        for status in self.GetAll():
            if status.value == value:
                return status

        return None

class GameState:
    def __init__(self, currentPlayer, status, displacedResourceData, bonusMarkersToPlaceAtEndOfTurn):
        self.currentPlayer = currentPlayer
        self.status = status
        self.displacedResourceData = displacedResourceData
        self.bonusMarkersToPlaceAtEndOfTurn = bonusMarkersToPlaceAtEndOfTurn

class Game:
    def __init__(self, id, name, creatorPlayerID):
        self.logger = logging.getLogger()
        self.id = id
        self.name = name
        self.creatorPlayerID = creatorPlayerID
        self.players = []
        self.numPlayers = 0
        self.currentPlayer = None
        self.minPlayerCount = 2
        self.maxPlayerCount = 5
        self.status = GameStatus.WaitingForMorePlayers
        self.board = None
        self.numCompletedCities = 0
        self.displacedResourceData = None
        self.bonusMarkersPool = []
        self.bonusMarkersToPlaceAtEndOfTurn = []

    def GetMetadataDict(self):
        return {
            'id': self.id,
            'name': self.name,
            'creatorPlayerID': self.creatorPlayerID,
            'currentPlayerID': self.currentPlayer.GetID() if self.currentPlayer is not None else None,
            'minPlayerCount': self.minPlayerCount,
            'maxPlayerCount': self.maxPlayerCount,
            'status': self.status.ToDict(),
            'players': [player.GetStateDict() for player in self.players]
        }

    def GetStateDict(self, playerID = None):
        metadataDict = self.GetMetadataDict()

        extraFieldsDict = {
            'completedCities': self.numCompletedCities,
            'displacedResourceData': self.displacedResourceData.GetStateDict() if self.displacedResourceData is not None else None,
            'bonusMarkersPool': [bonusMarker.GetStateDict() for bonusMarker in self.bonusMarkersPool],
            'bonusMarkersToPlaceAtEndOfTurn': [bonusMarker.GetStateDict() for bonusMarker in self.bonusMarkersToPlaceAtEndOfTurn],
            'boardState': self.board.GetStateDict(playerID) if self.board is not None else None
        }

        fullDict = metadataDict.copy()
        fullDict.update(extraFieldsDict)

        return fullDict

    @staticmethod
    def FromStateDict(stateDict):
        game = Game(stateDict['id'], stateDict['name'], stateDict['creatorPlayerID'])
        game.minPlayerCount = stateDict['minPlayerCount']
        game.maxPlayerCount = stateDict['maxPlayerCount']
        game.status = GameStatus().GetByValue(stateDict['status']['value'])
        game.players = [Player.FromStateDict(playerState) for playerState in stateDict['players']]
        game.currentPlayer = game._GetPlayerByID(stateDict['currentPlayerID'])
        game.numCompletedCities = stateDict['completedCities']
        game.bonusMarkersPool = [BonusMarker.FromStateDict(bonusMarkerState) for bonusMarkerState in stateDict['bonusMarkersPool']]
        game.bonusMarkersToPlaceAtEndOfTurn = [BonusMarker.FromStateDict(bonusMarkerState) for bonusMarkerState in stateDict['bonusMarkersToPlaceAtEndOfTurn']]
        game.board = Board.FromStateDict(stateDict['boardState']) if stateDict['boardState'] is not None else None
        game.numPlayers = len(game.players)
        return game

    def AddPlayer(self, playerID, preferredColorName = None):
        playerName = playerID
        availableColors = list(PlayerColor().GetAll())

        if self.status != GameStatus.ReadyToStart and self.status != GameStatus.WaitingForMorePlayers:
            errorMessage = "Game ID {} is in progress and can't accept more players".format(self.id)
            self.logger.error(errorMessage)
            return False, errorMessage

        if len(self.players) >= self.maxPlayerCount:
            errorMessage = "Game ID {} already has {} players".format(self.id, self.maxPlayerCount)
            self.logger.error(errorMessage)
            return False, errorMessage

        for player in self.players:
            availableColors.remove(player.GetColor())
            if player.GetID() == playerID:
                errorMessage = "{} is already in game ID {}".format(player, self.id)
                self.logger.error(errorMessage)
                return False, errorMessage

        if preferredColorName in (color.name for color in availableColors):
            color = PlayerColor().GetByName(preferredColorName)
        else:
            color = random.choice(availableColors)

        newPlayer = Player(playerID, playerName, color)
        self.players.append(newPlayer)

        if len(self.players) >= self.minPlayerCount:
            self.status = GameStatus.ReadyToStart

        successMessage = "Added player '{}' to game ID {} with color {}".format(playerName, self.id, color)
        self.logger.info(successMessage)
        return True, successMessage

    def Start(self):
        self.logger.info("Starting a new game.")
        self.numPlayers = len(self.players)

        if self.numPlayers < self.minPlayerCount:
            message = "At least {} players required to start the game. Add more players and try again.".format(self.minPlayerCount)
            self.logger.error(message)
            return False, message
        elif self.status != GameStatus.ReadyToStart:
            message = "Game ID {} has already started!".format(self.id)
            self.logger.error(message)
            return False, message
        else:
            self._SetupMainBoard()
            self._SetupPlayers()
            self.status = GameStatus.WaitingForCurrentPlayer
            message = "Game ID {} has begun!".format(self.id)
            self.logger.info(message)
            return True, message

    def _SetupMainBoard(self):
        self._InitBoardLayout()
        self._PlaceStartingBonusMarkersRandomly()
        self._ShuffleOtherBonusMarkers()
        self._PlaceCompletedCitiesMarker()

    def _InitBoardLayout(self):
        self.logger.info("Initializing board layout for %s players", self.numPlayers)
        self.board = Board()

        import os
        scriptDir = os.path.dirname(os.path.abspath(__file__))
        if self.numPlayers <= 3:
            self.board = BoardImporter().ImportFromFile(os.path.join(scriptDir, "..", "data", "DefaultBoard2-3.board"))
        else:
            self.board = BoardImporter().ImportFromFile(os.path.join(scriptDir, "..", "data", "DefaultBoard4-5.board"))

        self.logger.debug("%s", self.board.Output())

    def _PlaceStartingBonusMarkersRandomly(self):
        self.logger.info("Placing starting bonus markers randomly.")
        startingBonusMarkers = [
            AdditionalOfficeBonusMarker(),
            SwapOfficesBonusMarker(),
            RemoveResources3BonusMarker()]

        random.shuffle(startingBonusMarkers)

        self.board.AddBonusMarkersToStartingRoutes(startingBonusMarkers)

    def _ShuffleOtherBonusMarkers(self):
        self.logger.info("Shuffling other bonus markers.")
        self.bonusMarkersPool = [
            AdditionalOfficeBonusMarker(),
            AdditionalOfficeBonusMarker(),
            AdditionalOfficeBonusMarker(),
            SwapOfficesBonusMarker(),
            SwapOfficesBonusMarker(),
            ExtraActions3BonusMarker(),
            ExtraActions3BonusMarker(),
            ExtraActions4BonusMarker(),
            ExtraActions4BonusMarker(),
            IncreaseSkillBonusMarker(),
            IncreaseSkillBonusMarker(),
            RemoveResources3BonusMarker()]

        random.shuffle(self.bonusMarkersPool)

    def _PlaceCompletedCitiesMarker(self):
        self.numCompletedCities = 0

    def _SetupPlayers(self):
        self._AssignStartingPlayer()
        playerNum = 0

        for player in cycle_once_from_index(self.players, self.players.index(self.startingPlayer)):
            self.logger.info("Setting up %s as player %s", player, playerNum)
            player.Init(playerNum)
            playerNum += 1

    def _GetPlayerByID(self, playerID):
        for player in self.players:
            if player.GetID() == playerID:
                return player

        return None

    def _AssignStartingPlayer(self):
        self.startingPlayer = self.currentPlayer = random.choice(self.players)
        self.startingPlayer.SetStartingPlayer(True)
        self.logger.info("Starting player is %s", self.startingPlayer)

    def GetState(self):
        return GameState(self.currentPlayer, self.status, self.displacedResourceData, self.bonusMarkersToPlaceAtEndOfTurn)

    def DoPlayerAction(self, playerID, action):
        success = False
        message = ""

        player = self._GetPlayerByID(playerID)
        if player is None:
            message = "Player not found with ID %s" % playerID
            self.logger.error(message)

        elif self.displacedResourceData is not None:
            if player.GetID() == self.displacedResourceData.displacedResource.GetOwnerPlayerID() and action.GetType() == Actions.PlaceDisplacedResource:
                self.logger.info("%s is reacting to a displaced resource", player)
                success = True
                displacedResourceWasPlaced = False

                if len(action.resourceMovements) <= self.displacedResourceData.numExtraResources + 1:
                    for resourceMovement in action.resourceMovements:
                        # Check if the target field is adjacent.
                        if self.board.IsFieldOnNearestVacantRouteFromStartField(self.displacedResourceData.fieldID, resourceMovement.targetFieldID):
                            if not displacedResourceWasPlaced and resourceMovement.resourceType == self.displacedResourceData.displacedResource.GetType():
                                # Placing the displaced resource is free.
                                self.board.AddResourceToField(Resource(resourceMovement.resourceType, player.GetID()), resourceMovement.targetFieldID)
                                displacedResourceWasPlaced = True
                            else:
                                # Check if the player has enough resources in stock to place the extra resource.
                                if player.HasResourceInStock(resourceMovement.resourceType):
                                    if self.board.AddResourceToField(Resource(resourceMovement.resourceType, player.GetID()), resourceMovement.targetFieldID):
                                        player.RemoveResourceFromStock(resourceMovement.resourceType)
                                    else:
                                        success = False
                                # Failing that, the supply can be used, but only if the player's stock is completely empty.
                                elif player.IsStockEmpty():
                                    # Ok it's empty, check the supply
                                    if player.HasResourceInSupply(resourceMovement.resourceType):
                                        resourceToAdd = Resource(resourceMovement.resourceType, player.GetID())
                                        if self.board.AddResourceToField(resourceToAdd, resourceMovement.targetFieldID):
                                            player.RemoveResourceFromSupply(resourceMovement.resourceType)
                                        else:
                                            success = False
                                    elif player.IsSupplyEmpty():
                                        # If not, look for a sourceFieldID to move it from.
                                        sourceField = self.board.GetFieldByID(resourceMovement.sourceFieldID)
                                        if sourceField.GetID() == resourceMovement.sourceFieldID:
                                            success = self.board.MovePlayerResources(
                                                player,
                                                [resourceMovement.sourceFieldID],
                                                [resourceMovement.targetFieldID])
                                        else:
                                            message = "No resources in stock or supply, and no source field was specified to move from."
                                            self.logger.error(message)
                                            success = False
                                    else:
                                        message = "Since the stock is empty, extra resources must come from the supply"
                                        self.logger.error(message)
                                        success = False
                                else:
                                    message = "Must take extra resources from the stock since it's not empty"
                                    self.logger.error(message)
                                    success = False
                        else:
                            message = "Target field ID {} is not on the nearest vacant route from field ID {}".format(
                                resourceMovement.targetFieldID,
                                self.displacedResourceData.fieldID)
                            self.logger.error(message)
                            success = False

                    if not displacedResourceWasPlaced:
                        message = "You must at least place the displaced resource."
                        self.logger.error(message)
                        success = False

                    if success:
                        self.displacedResourceData = None
                        self.status = GameStatus.WaitingForCurrentPlayer
                else:
                    message = "You can only place {} extra resource(s) in response to a displaced {}".format(
                        self.displacedResourceData.numExtraResources,
                        self.displacedResourceData.displacedResource.GetType())
                    self.logger.error(message)
                    success = False
            else:
                message = "Waiting for player {} to move a {} displaced from field ID {}".format(
                    self.displacedResourceData.displacedResource.GetOwnerPlayerID(),
                    self.displacedResourceData.displacedResource.GetType(),
                    self.displacedResourceData.fieldID)
                self.logger.error(message)

        elif player != self.currentPlayer:
            message = "It's not {}'s turn!".format(player)
            self.logger.error(message)

        elif action.GetType() == Actions.UseBonusMarker:
            self.logger.info("%s is using bonus marker '%s'", player, action.bonusMarker)
            bonusMarker = player.GetUnusedBonusMarkerOfClassType(action.bonusMarker)
            if bonusMarker is not None:
                if action.bonusMarker.GetType() == BonusType.RemoveResources:
                    if action.bonusMarker.numResources >= len(action.bonusMarker.targetFieldIDs):
                        success, message = self.board.RemoveResources(action.bonusMarker.targetFieldIDs)
                    else:
                        message = "Trying to use 'Remove {} Resources' bonus marker but {} fields specified".format(
                            action.bonusMarker.numResources,
                            len(action.bonusMarker.targetFieldIDs))
                        self.logger.error(message)
                elif action.bonusMarker.GetType() == BonusType.AdditionalOffice:
                    message = "Additional office bonus marker must be used as part of an establish trade route action"
                    self.logger.error(message)
                elif action.bonusMarker.GetType() == BonusType.SwapOffices:
                    success = self.board.SwapAdjacentOfficesInCity(action.bonusMarker.cityID, action.bonusMarker.firstOfficeIndex)
                elif action.bonusMarker.GetType() == BonusType.ExtraActions:
                    player.AddActions(action.bonusMarker.numActions)
                    success = True
                elif action.bonusMarker.GetType() == BonusType.IncreaseSkill:
                    success, message = player.UpgradeSkill(action.bonusMarker.skillType)
                else:
                    message = "Trying to use an unknown bonus marker of type '{}'".format(action.bonusMarker.GetType())
                    self.logger.exception(message)

                if success:
                    bonusMarker.Use()
            else:
                message = "Player does not have an unused '{}' bonus marker".format(action.bonusMarker)
                self.logger.error(message)

        elif player.HasActions():
            self.logger.info("%s is taking action %s", player, action)

            if action.GetType() == Actions.Resupply:
                success, message = player.Resupply(action.numTraders, action.numMerchants)

            elif action.GetType() == Actions.PlaceResource:
                if player.HasResourceInSupply(action.resourceType):
                    if self.board.AddResourceToField(Resource(action.resourceType, player.GetID()), action.targetFieldID):
                        player.RemoveResourceFromSupply(action.resourceType)
                        success = True
                else:
                    message = "Not enough resources to perform this action."
                    self.logger.error(message)

            elif action.GetType() == Actions.DisplaceResource:
                field = self.board.GetFieldByID(action.targetFieldID)
                if field.GetID() == action.targetFieldID:
                    numPenaltyResourcesRequired = 1
                    if field.GetOccupyingResource().GetType() == ResourceType.Merchant:
                        numPenaltyResourcesRequired = 2
                    if action.numTradersPenalty + action.numMerchantsPenalty == numPenaltyResourcesRequired:
                        numTradersRequired = action.numTradersPenalty
                        numMerchantsRequired = action.numMerchantsPenalty

                        if action.placementResourceType == ResourceType.Trader:
                            numTradersRequired += 1
                        elif action.placementResourceType == ResourceType.Merchant:
                            numMerchantsRequired += 1

                        if player.HasResourcesInSupply(numTradersRequired, numMerchantsRequired):
                            success, displacedResource = self.board.ReplaceResourceInField(
                                Resource(action.placementResourceType, player.GetID()), action.targetFieldID)
                            if success:
                                player.RemoveResourceFromSupply(action.placementResourceType)
                                player.MoveResourcesFromSupplyToStock(action.numTradersPenalty, action.numMerchantsPenalty)
                                self.status = GameStatus.WaitingForPlayerReaction
                                self.displacedResourceData = DisplacedResourceData(displacedResource, action.targetFieldID, numPenaltyResourcesRequired)
                        else:
                            message = "Not enough resources to perform this action (requires {} traders and {} merchants.".format(
                                numTradersRequired,
                                numMerchantsRequired)
                            self.logger.error(message)
                    else:
                        message = "Wrong amount of penalty resources paid for this action (requires {}; received {} traders and {} merchants)".format(
                            numPenaltyResourcesRequired,
                            action.numTradersPenalty,
                            action.numMerchantsPenalty)
                        self.logger.error(message)
                else:
                    message = "Tried to displace a resource from invalid field ID {}".format(action.targetFieldID)
                    self.logger.error(message)

            elif action.GetType() == Actions.MoveResources:
                if player.GetMovementAllowance() >= len(action.sourceFieldIDs):
                    success = self.board.MovePlayerResources(player, action.sourceFieldIDs, action.targetFieldIDs)
                else:
                    message = "{} doesn't have enough ability to move {} pieces".format(player, len(action.sourceFieldIDs))
                    self.logger.error(message)

            elif action.GetType() == Actions.EstablishTradeRoute:
                # Check if route is full with only this player's resources.
                route = self.board.GetRouteByFieldID(action.routeFieldID)
                if route.ContainsFieldID(action.routeFieldID):
                    if route.IsFullOfOnePlayersResources(player):
                        city = route.GetCityByID(action.cityID)
                        controllingPlayerIDs = []
                        if city is not None:
                            # Remember the controlling players so they can be awarded points if this action succeeds.
                            for endPointCity in route.GetCities():
                                controllingPlayerID = endPointCity.GetControllingPlayerID()
                                if controllingPlayerID is not None:
                                    controllingPlayerIDs.append(controllingPlayerID)

                            if action.skillTypeToUpgrade is not None:
                                # Player wants to increase skill level provided by an endpoint city.
                                if city.HasSkillType(action.skillTypeToUpgrade):
                                    # Move one resource from the designated skill track to the personal supply.
                                    success, message = player.UpgradeSkill(action.skillTypeToUpgrade)
                                else:
                                    message = "City ID {} does not allow {} skill upgrades".format(action.cityID, action.skillTypeToUpgrade)
                                    self.logger.error(message)
                            else:
                                # Player wants to open an office in an endpoint city. Get the leftmost vacant office space in the endpoint city.
                                office = city.GetLeftmostVacantOffice()
                                # TODO: Need a way to specify which Coellen bonus space is being occupied since it's not left-to-right.
                                if office is not None:
                                    # Check if the player has the privilege level of the office
                                    if player.HasPrivilegeLevel(office.GetPrivilegeLevel()):
                                        # Check that the route had a resource of a type supported by that office
                                        resource = route.RemoveResourceOfType(office.GetResourceType())
                                        if resource is not None:
                                            # Occupy the office with that resource
                                            office.Occupy(resource)
                                            self.logger.info("%s occupied an office in %s", resource, city)
                                            # Award any points from that office
                                            if office.GetNumPointsToAwardOccupyingPlayer() > 0 and not office.HasPointsAwardedAtEndOfGame():
                                                player.AddPoints(office.GetNumPointsToAwardOccupyingPlayer())
                                            if city.IsFull():
                                                # Advance the completed cities counter that determines end-of-game
                                                self.numCompletedCities += 1
                                                self.logger.info("%s is now full (completed cities = %s)", city, self.numCompletedCities)
                                            # TODO: If this completes a network from Arnheim to Stendal, award 7 / 4 / 2 VP
                                            success = True
                                        else:
                                            message = "Office requires resource of type {} but there are none on the route".format(office.GetResourceType())
                                            self.logger.error(message)
                                    else:
                                        message = "Not enough privilege to occupy this office (requires {}, has {})".format(
                                            str(office.GetPrivilegeLevel()),
                                            str(player.GetPrivilegeLevel()))
                                        self.logger.error(message)
                                else:
                                    message = f"No vacant office in city ID {action.cityID}"
                                    self.logger.error(message)
                        if success:
                            # Award one prestige point to the controller of each endpoint city (determined before this player took an office).
                            for controllingPlayerID in controllingPlayerIDs:
                                controllingPlayer = self._GetPlayerByID(controllingPlayerID)
                                controllingPlayer.AddPoints(1)

                            # Take bonus marker if available and reserve a new one for the end of the turn.
                            bonusMarker = route.GetBonusMarker()
                            if bonusMarker is not None:
                                player.TakeBonusMarker(bonusMarker)
                                route.RemoveBonusMarker()
                                self._AddBonusMarkerToPlaceAtEndOfTurn()

                            # Return the remaining resources to stock
                            player.AddResourcesToStock(route.RemoveAllResources())

                    else:
                        message = "Route is not full"
                        self.logger.error(message)
                else:
                    message = f"No route found for field ID {action.routeFieldID}"
                    self.logger.exception(message)
            else:
                message = f"Invalid action '{action}'"
                self.logger.exception(message)

            if success:
                player.SpendAction()

                # Check for end of turn and end of game.
                if self._IsGameEndConditionMet():
                    self._AwardEndGamePoints()
                    self.Stop()
                elif not player.HasActions():
                    if len(self.bonusMarkersToPlaceAtEndOfTurn) <= 0:
                        self._EndPlayerTurn(player)

        elif action.GetType() == Actions.PlaceBonusMarkers and len(self.bonusMarkersToPlaceAtEndOfTurn) <= 0:
            message = "Attempting to place bonus markers, but none were acquired this turn"
            self.logger.exception(message)

        elif len(self.bonusMarkersToPlaceAtEndOfTurn) > 0:
            # Current player claimed a bonus marker during this turn and has a new one to place.
            if action.GetType() == Actions.PlaceBonusMarkers:
                success = self._PlaceBonusMarkers(action.targetFieldIDs)
            else:
                message = "Waiting for {} to place newly acquired bonus markers".format(self.currentPlayer)
                self.logger.error(message)

            if success:
                if len(self.bonusMarkersToPlaceAtEndOfTurn) <= 0:
                    self._EndPlayerTurn(player)

        return success, message

    def Stop(self):
        self.status = GameStatus.GameOver
        self.currentPlayer = None

        message = "Game ID {} has ended.".format(self.id)
        self.logger.info(message)

    def _RemoveRandomBonusMarkerFromPool(self):
        bonusMarker = None

        if len(self.bonusMarkersPool) > 0:
            bonusMarker = random.choice(self.bonusMarkersPool)
            self.bonusMarkersPool.remove(bonusMarker)
        else:
            self.logger.info("No more bonus markers in the pool!")

        return bonusMarker

    def _AddBonusMarkerToPlaceAtEndOfTurn(self):
        bonusMarker = self._RemoveRandomBonusMarkerFromPool()
        if bonusMarker is not None:
            self.bonusMarkersToPlaceAtEndOfTurn.append(bonusMarker)

    def _PlaceBonusMarkers(self, targetFieldIDs):
        if len(self.bonusMarkersToPlaceAtEndOfTurn) == len(targetFieldIDs):
            for bonusMarker, fieldID in zip(self.bonusMarkersToPlaceAtEndOfTurn, targetFieldIDs):
                route = self.board.GetRouteByFieldID(fieldID)
                if route.ContainsFieldID(fieldID):
                    if not route.HasBonusMarker():
                        if route.IsEmpty():
                            if route.HasVacantOfficeInAnEndpointCity():
                                route.AddBonusMarker(bonusMarker)
                                self.logger.info("Bonus marker %s added to route with field ID %s", bonusMarker, fieldID)
                            else:
                                self.logger.error("No empty office in an endpoint city")
                                return False
                        else:
                            self.logger.error("Route is not empty")
                            return False
                    else:
                        self.logger.error("Route already has a bonus marker")
                        return False
                else:
                    self.logger.error("No route found for field ID %s", fieldID)
                    return False
        else:
            self.logger.error(
                "Mismatch: There are %s bonus markers to place, but %s field IDs were specified",
                len(self.bonusMarkersToPlaceAtEndOfTurn),
                len(targetFieldIDs))
            return False

        self.bonusMarkersToPlaceAtEndOfTurn = []
        return True

    def _EndPlayerTurn(self, player):
        player.ResetActionCounter()
        self.currentPlayer = self._GetNextPlayer(self.currentPlayer)
        self.logger.info("%s's turn:", self.currentPlayer)

    def _GetNextPlayer(self, currentPlayer):
        for i, player in enumerate(self.players):
            if player == currentPlayer:
                nextPlayer = self.players[(i + 1) % self.numPlayers]
                break

        return nextPlayer

    def _IsGameEndConditionMet(self):
        return self._IsAnyPlayerAtNumPoints(20) or self.numCompletedCities >= 10 or len(self.bonusMarkersPool) <= 0

    def _IsAnyPlayerAtNumPoints(self, numPoints):
        for player in self.players:
            if player.GetPoints() >= numPoints:
                return True

        return False

    def _AwardEndGamePoints(self):
        # For each player, sum the following:
        finalScoreByPlayer = {}
        for player in self.players:
            self.logger.info("Final score for %s:", player)
            finalScoreByPlayer[player.GetID()] = 0
            # (a) VP on the board
            pointsOnBoard = player.GetPoints()
            finalScoreByPlayer[player.GetID()] += pointsOnBoard
            self.logger.info("%s VP on-board", pointsOnBoard)

            # (b) 4 VP per max skill
            numMasteredSkills = player.GetNumMasteredSkills()
            pointsFromMasteredSkills = numMasteredSkills * 4
            finalScoreByPlayer[player.GetID()] += pointsFromMasteredSkills
            self.logger.info("%s VP from %s mastered skills", pointsFromMasteredSkills, numMasteredSkills)

            # (c) Bonus marker points
            numBonusMarkers = player.GetNumBonusMarkers()
            pointsFromBonusMarkers = 0
            if numBonusMarkers > 9:
                pointsFromBonusMarkers = 21
            elif numBonusMarkers > 7:
                pointsFromBonusMarkers = 15
            elif numBonusMarkers > 5:
                pointsFromBonusMarkers = 10
            elif numBonusMarkers > 3:
                pointsFromBonusMarkers = 6
            elif numBonusMarkers > 1:
                pointsFromBonusMarkers = 3
            elif numBonusMarkers == 1:
                pointsFromBonusMarkers = 1

            finalScoreByPlayer[player] += pointsFromBonusMarkers
            self.logger.info("%s VP from %s bonus markers", pointsFromBonusMarkers, numBonusMarkers)

            # (d) Town Key skill value * num offices in largest network
            networkSize = self.board.GetNumOfficesInLargestNetworkForPlayer(player)
            townKeyValue = player.GetTownKeyValue()
            pointsFromNetwork = networkSize * townKeyValue
            finalScoreByPlayer[player.GetID()] += pointsFromNetwork
            self.logger.info("%s VP from network size %s and town key value %s", pointsFromNetwork, networkSize, townKeyValue)

        # (e) Points from cities (e.g. Coellen)
        # TODO: I think the Coellen spaces being treated like offices will mess up scoring calculations (d) and (f).
        for playerID, cityPoints in self.board.GetEndOfGameOfficePointsByPlayer().items():
            finalScoreByPlayer[playerID] += cityPoints
            self.logger.info("%s gets %s VP from special cities (e.g. Coellen)", playerID, cityPoints)

        # (f) 2 VP per controlled city
        for playerID, numControlledCities in self.board.GetNumControlledCitiesByPlayer().items():
            pointsFromControlledCities = numControlledCities * 2
            finalScoreByPlayer[playerID] += pointsFromControlledCities
            self.logger.info("%s gets %s VP from %s controlled cities", playerID, pointsFromControlledCities, numControlledCities)

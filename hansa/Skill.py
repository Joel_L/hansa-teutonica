import logging
import sys
from .Enum import Enum
from .Resource import ResourceType

class SkillTypes:
    TownKey = Enum("Town Keys", 0)
    Actions = Enum("Actions", 1)
    Privilege = Enum("Privilege", 2)
    BookOfLore = Enum("Book Of Lore", 3)
    Money = Enum("Money", 4)

    def GetAll(self):
        return [self.TownKey,
                self.Actions,
                self.Privilege,
                self.BookOfLore,
                self.Money]

    def GetByValue(self, value):
        for skillType in self.GetAll():
            if skillType.value == value:
                return skillType

class Skill:
    def __init__(self, skillType, resourceType, values):
        self.logger = logging.getLogger()
        self.type = skillType
        self.resourceType = resourceType
        self.values = values
        self.level = 0

    def GetStateDict(self):
        return {
            'type': self.type.ToDict(),
            'resourceType': self.resourceType.ToDict(),
            'level': self.GetLevel(),
            'values': self.values,
            'isMastered': self.IsMastered()
        }

    @staticmethod
    def FromStateDict(stateDict):
        skillType = SkillTypes().GetByValue(stateDict['type']['value'])
        resourceType = ResourceType().GetByName(stateDict['resourceType']['name'])
        skill = Skill(skillType, resourceType, stateDict['values'])
        skill.level = stateDict['level']
        return skill

    def Copy(self):
        return Skill.FromStateDict(self.GetStateDict())

    def GetType(self):
        return self.type

    def GetResourceType(self):
        return self.resourceType

    def GetLevel(self):
        return self.level

    def GetValue(self):
        return self.values[self.level]

    def IsMastered(self):
        return self.level == len(self.values) - 1

    def IncreaseLevel(self):
        self.level = self.level + 1

TownKeySkill = Skill(SkillTypes.TownKey, ResourceType.Trader, [1, 2, 2, 3, 4])
ActionSkill = Skill(SkillTypes.Actions, ResourceType.Trader, [2, 3, 3, 4, 4, 5])
PrivilegeSkill = Skill(SkillTypes.Privilege, ResourceType.Trader, [1, 2, 3, 4])
BookSkill = Skill(SkillTypes.BookOfLore, ResourceType.Merchant, [2, 3, 4, 5])
MoneyBagSkill = Skill(SkillTypes.Money, ResourceType.Trader, [3, 5, 7, sys.maxsize]) # Last level is "all".

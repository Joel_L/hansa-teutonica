import logging
import re
from .Board import Board
from .City import City
from .Office import Office
from .Resource import ResourceType
from .Route import Route
from .Field import Field
from .Skill import SkillTypes

class BoardImporter:
    def __init__(self):
        self.logger = logging.getLogger()

    def ImportFromFile(self, filename):
        self.board = Board()

        with open(filename, 'r') as boardFile:
            self.ParseLines(boardFile)

        return self.board

    def ParseLines(self, boardFile):
        for line in boardFile:
            line.strip()
            if len(line) > 0 and not line.startswith("#"):
                self.ParseLine(line)

    def ParseLine(self, line):
        if line.startswith("[C"):
            cityID, cityName, offices, skillTypes = self.ParseCityLine(line)
            self.board.AddCity(City(cityID, cityName, offices, skillTypes))
        elif line.startswith("[R"):
            fields, cities, hasTavern = self.ParseRouteLine(line)
            routeID = "{}-{}".format(cities[0].GetID(), cities[1].GetID())
            route = Route(routeID, fields, cities)
            self.board.AddRoute(route)
            if hasTavern:
                self.board.AddStartingRoute(route)

    def ParseCityLine(self, line):
        #                [C(1):"Groningen",[O:T1+1,M2],[S:B]]
        cityPattern = r'\[C\((\d+)\):"(.*)",\[O:(.*?)\](,\[S:(.)\])*\]'
        m = re.match(cityPattern, line)

        cityID = m.group(1)
        cityName = m.group(2)
        officesLine = m.group(3)
        skillsLine = m.group(5) # optional, could be None
        return cityID, cityName, self.ParseOfficesLine(cityID, officesLine), self.ParseSkillsLine(skillsLine)

    def ParseOfficesLine(self, cityID, line):
        offices = []
        for officeNum, officeLine in enumerate(line.split(",")):
            resourceCode = officeLine
            numPoints = 0
            if "+" in officeLine:
                resourceCode, numPoints = officeLine.split("+")
            numPoints = int(numPoints)

            resourceType = ResourceType.Vacant
            if resourceCode[0] == "T":
                resourceType = ResourceType.Trader
            elif resourceCode[0] == "M":
                resourceType = ResourceType.Merchant

            privilegeLevel = int(resourceCode[1])

            officeID = "{}.{}".format(cityID, officeNum)
            offices.append(Office(officeID, resourceType, privilegeLevel, numPoints, numPoints > 1))

        return offices

    def ParseSkillsLine(self, line):
        skills = []

        if line is not None:
            for skillCode in line.split(","):
                if skillCode == "T":
                    skills.append(SkillTypes.TownKey)
                elif skillCode == "A":
                    skills.append(SkillTypes.Actions)
                elif skillCode == "P":
                    skills.append(SkillTypes.Privilege)
                elif skillCode == "B":
                    skills.append(SkillTypes.BookOfLore)
                elif skillCode == "M":
                    skills.append(SkillTypes.Money)
                else:
                    self.logger.error("Unknown skill code '%s'", skillCode)

        return skills

    def ParseRouteLine(self, line):
        fields = []
        cities = []

        #                 [R:3-4,3,T]
        routePattern = r'\[R:(\d+)-(\d+?),(\d+)(,(.))*\]'
        m = re.match(routePattern, line)

        cityID1 = m.group(1)
        cityID2 = m.group(2)
        numFields = int(m.group(3))
        hasTavern = bool(m.group(5)) # optional, could be None

        for fieldNum in range(1, numFields + 1):
            fields.append(Field("{}-{}.{}".format(cityID1, cityID2, fieldNum)))

        cities.append(self.board.GetCityByID(cityID1))
        cities.append(self.board.GetCityByID(cityID2))

        return fields, cities, hasTavern

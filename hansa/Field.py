from copy import copy
from .Resource import Resource, ResourceType

class Field:
    def __init__(self, fieldID):
        self.id = fieldID
        self.occupyingResource = Resource(ResourceType.Vacant)

    def __str__(self):
        return self.id

    def GetStateDict(self):
        return {
            'id': self.id,
            'occupyingResource': self.occupyingResource.GetStateDict()
        }

    @staticmethod
    def FromStateDict(stateDict):
        field = Field(stateDict['id'])
        field.occupyingResource = Resource.FromStateDict(stateDict['occupyingResource'])
        return field

    def Output(self):
        text = "f" + self.id

        if self.occupyingResource.GetType() != ResourceType.Vacant:
            text += f"({self.occupyingResource})"

        return text

    def GetID(self):
        return self.id

    def GetOccupyingResource(self):
        return self.occupyingResource

    def IsOccupied(self):
        return self.occupyingResource.GetType() != ResourceType.Vacant

    def IsOccupiedBy(self, player):
        return self.occupyingResource.GetOwnerPlayerID() == player.GetID()

    def Occupy(self, resource):
        self.occupyingResource = resource

    def Vacate(self):
        vacatingResource = copy(self.occupyingResource)
        self.occupyingResource = Resource(ResourceType.Vacant)
        return vacatingResource

import logging
import sys

from .DBClient import DBClient
from .Game import Game

class GameManager:
    isInitialized = False
    _shared_state = {}

    def __init__(self, useTestDB = False):
        self.__dict__ = self._shared_state

        if not self.isInitialized:
            self.logger = logging.getLogger()
            logFormatter = logging.Formatter("%(asctime)s [%(module)-12.12s] [%(funcName)-25.25s] [%(lineno)-3.3d] [%(levelname)-5.5s]  %(message)s")
            logFormatter = logging.Formatter(fmt="{asctime} [{module:12.12s}] [{funcName:25.25s}] [{lineno:3d}] [{levelname:5.5s}]  {message}", style="{")
            self.consoleLogHandler = logging.StreamHandler(sys.stdout)
            self.consoleLogHandler.setFormatter(logFormatter)
            self.logger.setLevel(logging.DEBUG)
            self.logger.addHandler(self.consoleLogHandler)
            self.db = DBClient(useTestDB)

            self.games = []

            self.isInitialized = True

    def Shutdown(self):
        if self.isInitialized:
            self.db.DestroyDatabase()
            self.logger.removeHandler(self.consoleLogHandler)
            self.isInitialized = False

    def CreateGame(self, name, creatorPlayerID):
        gameID = self.db.GetNextGameID()

        game = Game(gameID, name, creatorPlayerID)

        self.db.InsertGame(game.GetStateDict())

        self.logger.info("Game ID %s created by player ID %s", game.id, creatorPlayerID)
        return game.id

    def GetAllGames(self):
        allGames = []

        for gameDoc in self.db.GetAllGames():
            game = Game.FromStateDict(gameDoc)
            allGames.append(game)

        return allGames

    def GetGameByID(self, gameID):
        gameDoc = self.db.GetGameByID(gameID)

        if gameDoc is None:
            return None

        game = Game.FromStateDict(gameDoc)
        return game

    def AddPlayerToGame(self, gameID, playerID):
        game = self.GetGameByID(gameID)

        if game is None:
            message = "Can't add player ID {} to game ID {} because it doesn't exist".format(playerID, gameID)
            self.logger.error(message)
            return False, message
        else:
            result = game.AddPlayer(playerID)
            self.db.UpdateGame(game.GetStateDict())
            return result

    def StartGame(self, gameID):
        game = self.GetGameByID(gameID)

        if game is None:
            message = "Can't start game ID {} because it doesn't exist".format(gameID)
            self.logger.error(message)
            return False, message
        else:
            result = game.Start()
            self.db.UpdateGame(game.GetStateDict())
            return result

    def DoPlayerAction(self, gameID, playerID, action):
        game = self.GetGameByID(gameID)

        if game is None:
            message = "Can't take an action in game ID {} because it doesn't exist".format(gameID)
            self.logger.error(message)
            return False, message
        else:
            result = game.DoPlayerAction(playerID, action)
            self.db.UpdateGame(game.GetStateDict())
            return result

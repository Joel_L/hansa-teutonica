import logging
from .Field import Field
from .City import City
from .BonusMarker import BonusMarker

class Route:
    def __init__(self, routeID, fields, cities):
        self.logger = logging.getLogger()
        self.id = routeID
        self.fields = fields
        self.cities = cities
        self.bonusMarker = None

    def __str__(self):
        return self.id

    def GetStateDict(self):
        return {
            'id': self.id,
            'fields': [field.GetStateDict() for field in self.fields],
            'cities': [city.GetStateDict() for city in self.cities],
            'bonusMarker': self.bonusMarker.GetStateDict() if self.bonusMarker is not None else None
        }

    @staticmethod
    def FromStateDict(stateDict):
        route = Route(stateDict['id'], None, None)
        route.fields = [Field.FromStateDict(fieldState) for fieldState in stateDict['fields']]
        route.cities = [City.FromStateDict(cityState) for cityState in stateDict['cities']]
        route.bonusMarker = BonusMarker.FromStateDict(stateDict['bonusMarker']) if stateDict['bonusMarker'] is not None else None
        return route

    def Output(self):
        fieldsAsStrings = []
        for field in self.fields:
            fieldsAsStrings.append(field.Output())
        return ("-" + "-".join(fieldsAsStrings) + "-").join([city.Output() for city in self.cities])

    def GetID(self):
        return self.id

    def GetFields(self):
        return self.fields

    def GetCities(self):
        return self.cities

    def GetCityByID(self, cityID):
        for city in self.cities:
            if city.GetID() == cityID:
                return city

        self.logger.error("City ID %s not found on this route", cityID)
        return None

    def AddBonusMarker(self, bonusMarker):
        self.bonusMarker = bonusMarker

    def HasBonusMarker(self):
        return self.bonusMarker is not None

    def GetBonusMarker(self):
        return self.bonusMarker

    def RemoveBonusMarker(self):
        self.bonusMarker = None

    def IsEmpty(self):
        for field in self.fields:
            if field.IsOccupied():
                return False

        return True

    def HasVacantOfficeInAnEndpointCity(self):
        for city in self.cities:
            if not city.IsFull():
                return True

        return False

    def IsFull(self):
        for field in self.fields:
            if not field.IsOccupied():
                return False

        return True

    def IsFullOfOnePlayersResources(self, player):
        for field in self.fields:
            if not field.IsOccupiedBy(player):
                return False

        return True

    def ContainsFieldID(self, fieldID):
        for field in self.fields:
            if field.GetID() == fieldID:
                return True

        return False

    def RemoveResourceOfType(self, resourceType):
        for field in self.fields:
            if field.GetOccupyingResource().GetType() == resourceType:
                return field.Vacate()

        return None

    def RemoveAllResources(self):
        removedResources = []

        for field in self.fields:
            if field.IsOccupied():
                removedResources.append(field.Vacate())

        return removedResources

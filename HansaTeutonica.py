# flake8: noqa

import logging
from hansa.Game import Game, GameStatus
from hansa.Action import *
from hansa.Resource import ResourceType
from hansa.Skill import SkillTypes
from hansa.BonusMarker import *

def _CreateUseBonusMarkerAction(bonusMarker):
    
    if bonusMarker.GetType() == BonusType.IncreaseSkill:
        skillType = SkillTypes.Privilege
        bonusMarker.PrepareForUse(skillType)
    
    elif bonusMarker.GetType() == BonusType.RemoveResources:
        targetFieldIDs = [0, 1, 2]
        bonusMarker.PrepareForUse(targetFieldIDs)
    
    elif bonusMarker.GetType() == BonusType.SwapOffices:
        cityID = 0
        firstOfficeIndex = 0
        bonusMarker.PrepareForUse(cityID, firstOfficeIndex)
    
    return UseBonusMarkerAction(bonusMarker)

playerActionSequence = [
    PlaceResourceAction(ResourceType.Trader, 0),
    PlaceResourceAction(ResourceType.Trader, 1),
    DisplaceResourceAction(ResourceType.Trader, 1, 0, 0),
    PlaceDisplacedResourceReaction([DisplacedResourceMovement(ResourceType.Trader, 3), DisplacedResourceMovement(ResourceType.Trader, 4)]),
    DisplaceResourceAction(ResourceType.Trader, 1, 0, 1),
    PlaceDisplacedResourceReaction([DisplacedResourceMovement(ResourceType.Trader, 5)]),
    _CreateUseBonusMarkerAction(ExtraActions3BonusMarker()),
    _CreateUseBonusMarkerAction(IncreaseSkillBonusMarker()),
    PlaceResourceAction(ResourceType.Trader, 6),
    PlaceResourceAction(ResourceType.Trader, 7),
    PlaceResourceAction(ResourceType.Trader, 8),
    EstablishTradeRouteAction(6, 0),
    ResupplyAction(1, 0),
    PlaceBonusMarkersAction([6]),
    _CreateUseBonusMarkerAction(ExtraActions4BonusMarker()),
    PlaceResourceAction(ResourceType.Trader, 6),
    PlaceResourceAction(ResourceType.Trader, 7),
    PlaceResourceAction(ResourceType.Trader, 8),
    EstablishTradeRouteAction(6, 0),
    ResupplyAction(1, 0),
    _CreateUseBonusMarkerAction(SwapOfficesBonusMarker()),    
    ResupplyAction(1, 0),
    PlaceBonusMarkersAction([6]),
    ResupplyAction(1, 0),
    ResupplyAction(1, 0),
    EstablishTradeRouteAction(4, 1, SkillTypes.Actions),
    ResupplyAction(1, 0),
    PlaceBonusMarkersAction([4]),
    _CreateUseBonusMarkerAction(RemoveResources3BonusMarker()),
    ]

def Main():
    game = Game()
    
    playerColors = ["Red", "Green", "Blue", "Yellow", "Purple"]
    for color in playerColors:
        playerName = "Mr " + str(color)
        game.AddPlayer(playerName, color)
        
    game.Start()
    
    _RunAutomatedGameLoop(game)
    
def _RunAutomatedGameLoop(game):
    gameStatus = game.GetStatus()
    while not gameStatus.state == GameStatus.GameOver and len(playerActionSequence) > 0:
        if gameStatus.displacedResourceData is not None:
            game.DoPlayerAction(gameStatus.displacedResourceData.displacedResource.GetOwnerPlayerID(), playerActionSequence.pop(0))
        else:
            game.DoPlayerAction(gameStatus.currentPlayer, playerActionSequence.pop(0))
        gameStatus = game.GetStatus()
    
    logging.getLogger().info("The game is over!") 

if __name__ == '__main__':
    Main()
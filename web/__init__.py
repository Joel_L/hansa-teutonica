import json
from flask import Flask
from flask import __version__
from flask import request
from flask import render_template
from flask import jsonify

from hansa.GameManager import GameManager
from hansa.Action import PlaceResourceAction
from hansa.Action import MoveResourcesAction
from hansa.Resource import ResourceType

# For Flask to work with Vue.js, since they both use {{ }} notation,
# change Flask's delimiters to $$.
class CustomFlask(Flask):
    jinja_options = Flask.jinja_options.copy()
    jinja_options.update(dict(
        block_start_string='$$',
        block_end_string='$$',
        variable_start_string='$',
        variable_end_string='$',
        comment_start_string='$#',
        comment_end_string='#$'))

    def GetVersionString(self):
        return "Flask " + __version__

app = CustomFlask(__name__)

@app.route('/')
def show_index():
    return render_template('index.html')

@app.route('/game/<int:gameID>')
def show_game_page(gameID):
    return render_template('game.html')

@app.route('/api/games', methods=['GET', 'POST'])
def do_games():
    if request.method == 'GET':
        return find_games()
    if request.method == 'POST':
        return create_game(request.form['gameName'])

def find_games():
    resp = jsonify({'games': [game.GetMetadataDict() for game in GameManager().GetAllGames()]})
    return resp

def create_game(gameName):
    creatorPlayerID = request.form['creatorPlayerID']
    if GameManager().CreateGame(gameName, creatorPlayerID):
        resp = ("Game created", 201, {'Location': "/path/to/game/123"}) # Fill in the Location header and use it [HTG-73]
    else:
        resp = ("Failed to create game.", 409)

    return resp

@app.route('/api/game/<int:gameID>/players', methods=['POST'])
def add_player_to_game(gameID):
    playerID = request.form['playerID']
    success, message = GameManager().AddPlayerToGame(gameID, playerID)

    if success:
        resp = (message, 200)
    else:
        resp = (message, 409)

    return resp

@app.route('/api/game/<int:gameID>/state', methods=['GET'])
def get_game_state(gameID):
    playerID = request.values['playerID']
    game = GameManager().GetGameByID(gameID)

    if game is not None:
        resp = jsonify({'gamestate': game.GetStateDict(playerID)})
    else:
        resp = ("Game ID {} not found".format(gameID), 404)

    return resp

@app.route('/api/game/<int:gameID>/actions', methods=['POST'])
def do_game_action(gameID):
    playerID = request.form['playerID']
    actionName = request.form['actionName']

    if actionName == "StartGame":
        success, message = GameManager().StartGame(gameID)
    elif actionName == "PlaceResource":
        resourceTypeName = request.form['resourceType']
        resourceType = ResourceType().GetByName(resourceTypeName)
        targetFieldID = request.form['targetFieldID']
        action = PlaceResourceAction(resourceType, targetFieldID)
        success, message = GameManager().DoPlayerAction(gameID, playerID, action)
    elif actionName == "MoveResources":
        sourceFieldIDs = json.loads(request.form['sourceFieldIDs'])
        targetFieldIDs = json.loads(request.form['targetFieldIDs'])
        action = MoveResourcesAction(sourceFieldIDs, targetFieldIDs)
        success, message = GameManager().DoPlayerAction(gameID, playerID, action)
    else:
        success = False
        message = "Unknown action '{}'".format(actionName)

    if success:
        resp = (message, 200)
    else:
        resp = (message, 409)

    return resp

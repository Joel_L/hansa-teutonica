new Vue
({
    el: '#htg_game',

    data:
    {
        // Constants
        spriteWidth: 30,

        isDebugEnabled: false,
        loggedInPlayerID: "Joel",
        gameState: {},
        currentPlayer: "",
        statusMessage: "",
        statusTooltip: "status message",
        selectedResourceLocationID: "",
        pendingMoveDestinationFieldIDs: new Set(),
        locations: [
            // Fields
            { id: "1-2.1", position: { left: 139, top: 200 }, isOccupied: false },
            { id: "1-2.2", position: { left: 206, top: 186 }, isOccupied: false },
            { id: "1-2.3", position: { left: 274, top: 151 }, isOccupied: false },

            { id: "2-3.1", position: { left: 363, top: 188 }, isOccupied: false },
            { id: "2-3.2", position: { left: 318, top: 228 }, isOccupied: false },
            { id: "2-3.3", position: { left: 280, top: 276 }, isOccupied: false },
            { id: "2-3.4", position: { left: 324, top: 316 }, isOccupied: false },

            { id: "3-4.1", position: { left: 404, top: 329 }, isOccupied: false },
            { id: "3-4.2", position: { left: 460, top: 278 }, isOccupied: false },
            { id: "3-4.3", position: { left: 525, top: 236 }, isOccupied: false },

            { id: "3-15.1", position: { left: 227, top: 354 }, isOccupied: false },
            { id: "3-15.2", position: { left: 140, top: 338 }, isOccupied: false },

            { id: "4-5.1", position: { left: 710, top: 204 }, isOccupied: false },
            { id: "4-5.2", position: { left: 788, top: 193 }, isOccupied: false },
            { id: "4-5.3", position: { left: 856, top: 165 }, isOccupied: false },
            { id: "4-5.4", position: { left: 905, top: 114 }, isOccupied: false },

            { id: "4-10.1", position: { left: 654, top: 249 }, isOccupied: false },
            { id: "4-10.2", position: { left: 689, top: 284 }, isOccupied: false },
            { id: "4-10.3", position: { left: 720, top: 321 }, isOccupied: false },

            { id: "4-13.1", position: { left: 594, top: 260 }, isOccupied: false },
            { id: "4-13.2", position: { left: 541, top: 321 }, isOccupied: false },
            { id: "4-13.3", position: { left: 510, top: 388 }, isOccupied: false },

            { id: "5-6.1", position: { left: 846, top:  73 }, isOccupied: false },
            { id: "5-6.2", position: { left: 792, top: 110 }, isOccupied: false },
            { id: "5-6.3", position: { left: 729, top: 124 }, isOccupied: false },

            { id: "5-7.1", position: { left: 1038, top:  87 }, isOccupied: false },
            { id: "5-7.2", position: { left: 1089, top: 130 }, isOccupied: false },
            { id: "5-7.3", position: { left: 1157, top: 120 }, isOccupied: false },

            { id: "5-9.1", position: { left:  967, top: 108 }, isOccupied: false },
            { id: "5-9.2", position: { left: 1003, top: 151 }, isOccupied: false },
            { id: "5-9.3", position: { left: 1042, top: 187 }, isOccupied: false },
            { id: "5-9.4", position: { left: 1071, top: 232 }, isOccupied: false },

            { id: "8-9.1", position: { left: 1159, top: 272 }, isOccupied: false },
            { id: "8-9.2", position: { left: 1101, top: 298 }, isOccupied: false },
            { id: "8-9.3", position: { left: 1040, top: 310 }, isOccupied: false },

            { id: "8-12.1", position: { left: 1245, top: 271 }, isOccupied: false },
            { id: "8-12.2", position: { left: 1237, top: 334 }, isOccupied: false },
            { id: "8-12.3", position: { left: 1220, top: 390 }, isOccupied: false },

            { id: "9-10.1", position: { left: 937, top: 300 }, isOccupied: false },
            { id: "9-10.2", position: { left: 898, top: 331 }, isOccupied: false },
            { id: "9-10.3", position: { left: 846, top: 361 }, isOccupied: false },

            { id: "10-13.1", position: { left: 766, top: 409 }, isOccupied: false },
            { id: "10-13.2", position: { left: 712, top: 412 }, isOccupied: false },
            { id: "10-13.3", position: { left: 668, top: 430 }, isOccupied: false },

            { id: "11-12.1", position: { left:  982, top: 442 }, isOccupied: false },
            { id: "11-12.2", position: { left: 1023, top: 474 }, isOccupied: false },
            { id: "11-12.3", position: { left: 1069, top: 503 }, isOccupied: false },
            { id: "11-12.4", position: { left: 1116, top: 530 }, isOccupied: false },

            { id: "11-13.1", position: { left: 856, top: 456 }, isOccupied: false },
            { id: "11-13.2", position: { left: 800, top: 474 }, isOccupied: false },
            { id: "11-13.3", position: { left: 744, top: 483 }, isOccupied: false },
            { id: "11-13.4", position: { left: 684, top: 485 }, isOccupied: false },

            { id: "12-22.1", position: { left: 1218, top: 523 }, isOccupied: false },
            { id: "12-22.2", position: { left: 1213, top: 574 }, isOccupied: false },
            { id: "12-22.3", position: { left: 1239, top: 631 }, isOccupied: false },

            { id: "13-14.1", position: { left: 500, top: 502 }, isOccupied: false },
            { id: "13-14.2", position: { left: 453, top: 502 }, isOccupied: false },
            { id: "13-14.3", position: { left: 412, top: 541 }, isOccupied: false },

            { id: "13-19.1", position: { left: 598, top: 513 }, isOccupied: false },
            { id: "13-19.2", position: { left: 585, top: 559 }, isOccupied: false },
            { id: "13-19.3", position: { left: 570, top: 599 }, isOccupied: false },

            { id: "14-16.1", position: { left: 269, top: 521 }, isOccupied: false },
            { id: "14-16.2", position: { left: 231, top: 498 }, isOccupied: false },
            { id: "14-16.3", position: { left: 183, top: 483 }, isOccupied: false },

            { id: "15-16.1", position: { left: 44, top: 336 }, isOccupied: false },
            { id: "15-16.2", position: { left: 71, top: 370 }, isOccupied: false },
            { id: "15-16.3", position: { left: 98, top: 414 }, isOccupied: false },

            { id: "16-17.1", position: { left:  73, top: 529 }, isOccupied: false },
            { id: "16-17.2", position: { left: 124, top: 561 }, isOccupied: false },
            { id: "16-17.3", position: { left: 141, top: 615 }, isOccupied: false },

            { id: "17-18.1", position: { left: 165, top: 670 }, isOccupied: false },
            { id: "17-18.2", position: { left: 216, top: 669 }, isOccupied: false },

            { id: "18-19.1", position: { left: 396, top: 673 }, isOccupied: false },
            { id: "18-19.2", position: { left: 454, top: 684 }, isOccupied: false },
            { id: "18-19.3", position: { left: 508, top: 657 }, isOccupied: false },

            { id: "19-20.1", position: { left: 653, top: 666 }, isOccupied: false },
            { id: "19-20.2", position: { left: 710, top: 645 }, isOccupied: false },
            { id: "19-20.3", position: { left: 754, top: 619 }, isOccupied: false },

            { id: "19-26.1", position: { left: 584, top: 732 }, isOccupied: false },
            { id: "19-26.2", position: { left: 547, top: 764 }, isOccupied: false },
            { id: "19-26.3", position: { left: 502, top: 796 }, isOccupied: false },

            { id: "20-21.1", position: { left: 865, top: 565 }, isOccupied: false },
            { id: "20-21.2", position: { left: 911, top: 577 }, isOccupied: false },
            { id: "20-21.3", position: { left: 961, top: 593 }, isOccupied: false },

            { id: "21-22.1", position: { left: 1077, top: 645 }, isOccupied: false },
            { id: "21-22.2", position: { left: 1127, top: 674 }, isOccupied: false },

            { id: "21-23.1", position: { left: 950, top: 653 }, isOccupied: false },
            { id: "21-23.2", position: { left: 897, top: 671 }, isOccupied: false },
            { id: "21-23.3", position: { left: 909, top: 727 }, isOccupied: false },
            { id: "21-23.4", position: { left: 957, top: 750 }, isOccupied: false },

            { id: "23-24.1", position: { left: 1053, top: 791 }, isOccupied: false },
            { id: "23-24.2", position: { left: 1077, top: 829 }, isOccupied: false },
            { id: "23-24.3", position: { left: 1117, top: 854 }, isOccupied: false },
            { id: "23-24.4", position: { left: 1166, top: 856 }, isOccupied: false },

            { id: "23-25.1", position: { left: 984, top: 802 }, isOccupied: false },
            { id: "23-25.2", position: { left: 897, top: 814 }, isOccupied: false },
            { id: "23-25.3", position: { left: 824, top: 844 }, isOccupied: false },

            { id: "26-27.1", position: { left: 358, top: 841 }, isOccupied: false },
            { id: "26-27.2", position: { left: 275, top: 843 }, isOccupied: false },
            { id: "26-27.3", position: { left: 198, top: 843 }, isOccupied: false },
            { id: "26-27.4", position: { left: 126, top: 843 }, isOccupied: false },

            // Offices
        ]
    },

    // called after view is loaded (see https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)
    mounted: function()
    {
        this.RefreshGameState();
    },
  
    methods:
    {
        ToggleDebug: function()
        {
            this.isDebugEnabled = !this.isDebugEnabled;
        },

        HandleError: function(error)
        {
            this.statusMessage = "Error: " + error.responseText
        },

        GetGameID: function()
        {
            // Extract the game ID from the URL
            const segments = new URL(window.location.href).pathname.split('/');
            const last = segments.pop() || segments.pop(); // Handle potential trailing slash

            return last;
        },
        
        // This requests the complete game state and replaces whatever is currently displayed on the board.
        RefreshGameState: function()
        {
            const vm = this
            const url = "/api/game/" + this.GetGameID() + "/state";
            $.get(url, { playerID: this.loggedInPlayerID })
                .done(function(response)
                {
                    vm.gameState = response.gamestate;
                    console.log(vm.gameState);

                    for (let field of vm.gameState.boardState.fields)
                    {
                        vm.AddResourceToField(field.id, field.occupyingResource);
                    }

                    // During development, the loggedInPlayer is always the current player.
                    vm.currentPlayer = vm.GetPlayerByID(vm.gameState.currentPlayerID);
                    vm.loggedInPlayerID = vm.currentPlayer.id;

                    // Convert skills to a dict for easier access.
                    vm.currentPlayer.skillsValue = Object();
                    for (let skill of vm.currentPlayer.skills)
                    {
                        vm.currentPlayer.skillsValue[skill.type.name.replaceAll(" ", "")] = skill.values[skill.level];
                    }

                    // This field will track pending resource movements for the Move action.
                    vm.pendingMoveDestinationFieldIDs = new Set();
                })
                .fail(this.HandleError)
        },

        GetPlayerByID: function(playerID)
        {
            for (let player of this.gameState.players)
            {
                if (player.id == playerID)
                {
                    return player;
                }
            }

            return null;
        },

        OnResourceSelected: function(locationID)
        {
            this.selectedResourceLocationID = locationID;
            this.statusMessage = "Selected resource at " + locationID;
        },

        OnLocationClicked: function(locationID)
        {
            this.statusMessage = "Clicked location " + locationID;

            if (this.selectedResourceLocationID != "")
            {
                this.DoSubAction_MoveSelectedResourceToLocation(locationID);
                this.selectedResourceLocationID = "";
            }
            else
            {
                this.DoAction_PlaceResource(locationID);
            }
        },

        OnSubmitMoves: function()
        {
            let sourceFieldIDs = [];
            let destinationFieldIDs = [];

            for (let destinationFieldID of this.pendingMoveDestinationFieldIDs.values())
            {
                let destinationFieldLocation = this.GetLocationByID(destinationFieldID);

                sourceFieldIDs.push(destinationFieldLocation.occupyingResource.moveSourceFieldID);
                destinationFieldIDs.push(destinationFieldID);
            }

            this.DoAction_MoveResources(sourceFieldIDs, destinationFieldIDs);
        },

        OnCancelMoves: function()
        {
            this.ResetPendingMoves();
        },

        IsResourceSelectedAtLocation: function(locationID)
        {
            return this.selectedResourceLocationID == locationID;
        },

        IsResourcePendingMoveAtLocation: function(locationID)
        {
            return this.pendingMoveDestinationFieldIDs.has(locationID);
        },

        IsAnyMovePending: function()
        {
            return this.pendingMoveDestinationFieldIDs?.size > 0;
        },

        ClearPendingMoves: function()
        {
            for (let fieldID of this.pendingMoveDestinationFieldIDs)
            {
                let field = this.GetLocationByID(fieldID);
                field.occupyingResource.moveSourceFieldID = null;
            }

            this.pendingMoveDestinationFieldIDs.clear();
        },

        ResetPendingMoves: function()
        {
            let resources = [];
            for (let fieldID of this.pendingMoveDestinationFieldIDs)
            {
                resources.push(this.RemoveResourceFromField(fieldID));
            }

            for (let resource of resources)
            {
                this.AddResourceToField(resource.moveSourceFieldID, resource);

                resource.moveSourceFieldID = null;
            }

            this.pendingMoveDestinationFieldIDs.clear();
        },

        GetLocationByID: function(locationID)
        {
            for (let location of this.locations)
            {
                if (location.id == locationID)
                {
                    return location;
                }
            }

            return null;
        },

        AddResourceToField: function(fieldID, resource)
        {
            let fieldLocation = this.GetLocationByID(fieldID);
            fieldLocation.isOccupied = true;
            fieldLocation.occupyingResource = resource;
        },

        RemoveResourceFromField: function(fieldID)
        {
            let fieldLocation = this.GetLocationByID(fieldID);
            let resource = fieldLocation.occupyingResource;
            fieldLocation.isOccupied = false;
            fieldLocation.occupyingResource = null;
            return resource;
        },

        GetSpriteStyleForResource: function(resource)
        {
            let ownerPlayer = this.GetPlayerByID(resource.ownerID);
            let spriteOffsetX = ownerPlayer.color.value * this.spriteWidth;

            return {
                backgroundPositionX: -spriteOffsetX + "px"
            }
        },

        DoAction_PlaceResource: function(locationID)
        {
            let vm = this;
            $.post("/api/game/" + this.GetGameID() + "/actions",
            {
                playerID: this.loggedInPlayerID,
                actionName: "PlaceResource",
                resourceType: "trader",
                targetFieldID: locationID
            })
            .done(function(response)
            {
                vm.statusMessage = response;
                vm.RefreshGameState();
            })
            .fail(this.HandleError)            
        },

        DoSubAction_MoveSelectedResourceToLocation: function(fieldID)
        {
            let destinationField = this.GetLocationByID(fieldID)
            if (destinationField.isOccupied)
            {
                this.statusMessage = "Move: Destination must be empty";
                return;
            }

            let sourceField = this.GetLocationByID(this.selectedResourceLocationID);
            if (sourceField.occupyingResource.ownerID != this.currentPlayer.id)
            {
                this.statusMessage = "Move: Can only move your own resources";
                return;
            }

            if (this.pendingMoveDestinationFieldIDs.size == this.currentPlayer.skillsValue.BookOfLore)
            {
                if (!this.pendingMoveDestinationFieldIDs.isSupersetOf(new Set([this.selectedResourceLocationID])))
                {
                    this.statusMessage = `Move: Maximum number of moves is ${this.currentPlayer.skillsValue.BookOfLore} (Book skill)`;
                    return;    
                }
            }

            let resource = this.RemoveResourceFromField(this.selectedResourceLocationID);
            if (resource.moveSourceFieldID == null)
            {
                resource.moveSourceFieldID = sourceField.id;
            }

            this.AddResourceToField(fieldID, resource);
            this.pendingMoveDestinationFieldIDs.delete(this.selectedResourceLocationID);

            // Detect a resource returning to its starting position and don't add it to the list.
            if (resource.moveSourceFieldID != fieldID)
            {
                this.pendingMoveDestinationFieldIDs.add(fieldID);
            }
                
            this.statusMessage = `Move: ${resource.ownerID}'s ${resource.type} moved to ${fieldID}`;
        },

        DoAction_MoveResources: function(sourceFieldIDs, targetFieldIDs)
        {
            let vm = this;
            $.post("/api/game/" + this.GetGameID() + "/actions",
            {
                contentType: 'application/json',
                playerID: this.loggedInPlayerID,
                actionName: "MoveResources",
                sourceFieldIDs: JSON.stringify(sourceFieldIDs),
                targetFieldIDs: JSON.stringify(targetFieldIDs)
            })
            .done(function(response)
            {
                vm.statusMessage = response;
                vm.ClearPendingMoves();
                vm.RefreshGameState();
            })
            .fail(this.HandleError)            
        },
    },
})

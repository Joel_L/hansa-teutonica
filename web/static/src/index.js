new Vue
({
    el: '#app',

    data:
    {
        loggedInPlayerID: "Joel",
        playerIDHint: "Enter your player ID",
        gameName: "test game",
        gameCreationHint: "Enter the name of the game you want to create",
        statusMessage: "",
        tooltip: "status message",
        games: [],
    },

    // properties that will auto-update if any of their data changes
    computed:
    {
        reversedMessage: function()
        {
            // `this` points to the vm (Vue) instance
            return this.statusMessage.split('').reverse().join('')    
        }
    },

    // called after view is loaded (see https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)
    mounted: function()
    {
        this.ListGames();
    },
  
    methods:
    {
        HandleError: function(error)
        {
            this.statusMessage = "Error: " + error.responseText
        },

        ListGames: function()
        {
            var vm = this            
            $.get("/api/games")
                .done(function(response)
                {
                    vm.games = response.games;
                    console.log(vm.games)
                })
                .fail(this.HandleError)
        },

        ShouldShowGameOptions: function(game)
        {
            return (game.creatorPlayerID == this.loggedInPlayerID ||
                    game.players.findIndex(player => player.id == this.loggedInPlayerID) == -1);
        },

        ShouldShowJoin: function(game)
        {
            return game.players.findIndex(player => player.id == this.loggedInPlayerID) == -1;
        },

        ShouldShowStart: function(game)
        {
            return game.creatorPlayerID == this.loggedInPlayerID;
        },

        ShouldShowDelete: function(game)
        {
            return game.creatorPlayerID == this.loggedInPlayerID;
        },

        CreateGame: function()
        {
            var vm = this;
            $.post("/api/games", { gameName: this.gameName, creatorPlayerID: this.loggedInPlayerID })
                .done(function(response)
                {
                    vm.statusMessage = response;
                    vm.ListGames();
                })
                .fail(this.HandleError)
        },

        AddPlayer: function(gameID)
        {
            var vm = this;
            $.post("/api/game/" + gameID + "/players", { playerID: this.loggedInPlayerID })
                .done(function(response)
                {
                    vm.statusMessage = response;
                    vm.ListGames();
                })
                .fail(this.HandleError)
        },

        StartGame: function(gameID)
        {
            var vm = this;
            $.post("/api/game/" + gameID + "/actions",
            {
                playerID: this.loggedInPlayerID,
                actionName: "StartGame"
            })
            .done(function(response)
            {
                vm.statusMessage = response;
                vm.ListGames();
            })
            .fail(this.HandleError)            
        },

        GetUrlForGame: function(gameID)
        {
            return "/game/" + gameID;
        },
    },

    // methods that can modify data in-line with '|'
    filters:
    {
        extractNames: function(namedItems)
        {
            return namedItems.map(namedItem => namedItem.name).join(", ")
        }
    },

    // methods that are called whenever the specified data changes
    watch:
    {    
        gameName: function(newGameName)
        {
            if (this.gameName.length < 3)
            {
                this.statusMessage = "Game name must be at least 3 characters long"
            }
            else
            {
                this.statusMessage = ""
            }
        }
    }  
})

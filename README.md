**This is a work in progress**

This project is an unofficial online implementation of Hansa Teutonica.

Issue tracker:
https://joel-lieberman.atlassian.net/browse/HTG

## INSTALL

1. Clone the repro
1. Install [Python 3.9.5](https://www.python.org/downloads/release/python-395/)
1. `python -m pip install -r requirements.txt`
1. Install [MongoDB](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)

## RUN

1. `python ./startweb.py`
2. Open http://localhost:5000

## TEST

1. `coverage run ./tests/runtests.py`
2. `coverage report (to see test coverage)`
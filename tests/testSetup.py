if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from hansa.GameManager import GameManager
from hansa.Player import PlayerColor
from hansa.Skill import SkillTypes

import unittest

class SetupTestCase(unittest.TestCase):
    def setUp(self):
        self.gameManager = GameManager(True)
        id = self.gameManager.CreateGame("TestGame", "Mr Test")
        self.game = GameManager().GetGameByID(id)

        for playerColor in PlayerColor().GetAll():
            playerName = "Mr " + str(playerColor).capitalize()
            self.game.AddPlayer(playerName, str(playerColor))

        self.game.Start()

    def tearDown(self):
        self.gameManager.Shutdown()

    def testSetup_BonusMarkers(self):
        bonusMarkerCount = 0
        expectedNumBonusMarkers = 3

        for route in self.game.board.routes:
            if route.HasBonusMarker():
                bonusMarkerCount += 1

        self.assertEqual(bonusMarkerCount, expectedNumBonusMarkers)

    def testSetup_PlayerResources(self):
        maxNumTradersInStartingStock = 6
        minNumTradersInStartingSupply = 5

        for player in self.game.players:
            self.assertEqual(player.numPoints, 0)

            self.assertEqual(player.stockResourcePool.numTraders, maxNumTradersInStartingStock - player.GetNumber())
            self.assertEqual(player.stockResourcePool.numMerchants, 0)

            self.assertEqual(player.personalSupplyResourcePool.numTraders, minNumTradersInStartingSupply + player.GetNumber())
            self.assertEqual(player.personalSupplyResourcePool.numMerchants, 1)

    def testSetup_PlayerSkills(self):
        for player in self.game.players:
            self.assertEqual(player._GetSkill(SkillTypes.TownKey).GetValue(), 1)
            self.assertEqual(player._GetSkill(SkillTypes.Actions).GetValue(), 2)
            self.assertEqual(player._GetSkill(SkillTypes.Privilege).GetValue(), 1)
            self.assertEqual(player._GetSkill(SkillTypes.BookOfLore).GetValue(), 2)
            self.assertEqual(player._GetSkill(SkillTypes.Money).GetValue(), 3)

def TheTestSuite():
    return unittest.defaultTestLoader.loadTestsFromTestCase(SetupTestCase)

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(TheTestSuite())

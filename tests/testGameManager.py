if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from hansa.GameManager import GameManager
from hansa.Player import PlayerColor
from hansa.Action import ResupplyAction

import unittest

class GameManagerTestCase(unittest.TestCase):
    def setUp(self):
        self.gameManager = GameManager(True)

    def tearDown(self):
        self.gameManager.Shutdown()

    def testCreateOneGame(self):
        id = self.gameManager.CreateGame("TestGame", "Mr Test")
        game = self.gameManager.GetGameByID(id)

        self.assertEqual(game.id, id)

    def testCreateTwoGames(self):
        id1 = self.gameManager.CreateGame("TestGame 1", "Mr Test 1")
        game1 = self.gameManager.GetGameByID(id1)

        id2 = self.gameManager.CreateGame("TestGame 2", "Mr Test 2")
        game2 = self.gameManager.GetGameByID(id2)

        self.assertNotEqual(game1.id, game2.id)

    def testCreateTwoGames_SameName(self):
        id1 = self.gameManager.CreateGame("TestGame", "Mr Test 1")
        game1 = self.gameManager.GetGameByID(id1)

        id2 = self.gameManager.CreateGame("TestGame", "Mr Test 2")
        game2 = self.gameManager.GetGameByID(id2)

        self.assertNotEqual(game1.id, game2.id)

    def testCreateTwoGames_SameCreator(self):
        id1 = self.gameManager.CreateGame("TestGame 1", "Mr Test")
        game1 = self.gameManager.GetGameByID(id1)

        id2 = self.gameManager.CreateGame("TestGame 2", "Mr Test")
        game2 = self.gameManager.GetGameByID(id2)

        self.assertNotEqual(game1.id, game2.id)

    def testGetAllGames(self):
        id1 = self.gameManager.CreateGame("TestGame 1", "Mr Test")
        game1 = self.gameManager.GetGameByID(id1)

        id2 = self.gameManager.CreateGame("TestGame 1", "Mr Test")
        game2 = self.gameManager.GetGameByID(id2)

        games = self.gameManager.GetAllGames()

        self.assertEqual(len([game for game in games if game.id == game1.id]), 1)
        self.assertEqual(len([game for game in games if game.id == game2.id]), 1)

    def testCreateManyGames(self):
        numGames = 1000

        for i in range(numGames):
            self.gameManager.CreateGame("TestGame " + str(i), "Mr Test " + str(i))

        games = self.gameManager.GetAllGames()

        self.assertEqual(len(games), numGames)

    def testAddPlayerToGame(self):
        gameID = self.gameManager.CreateGame("TestGame", "Mr Test")

        playerColor = PlayerColor.Blue
        playerID = "Mr " + str(playerColor).capitalize()

        success = self.gameManager.AddPlayerToGame(gameID, playerID)[0]
        self.assertTrue(success)

    def testAddPlayerToGame_InvalidID(self):
        gameID = 123456789

        playerColor = PlayerColor.Blue
        playerID = "Mr " + str(playerColor).capitalize()

        success = self.gameManager.AddPlayerToGame(gameID, playerID)[0]
        self.assertFalse(success)

    def testStartGame(self):
        gameID = self.gameManager.CreateGame("TestGame", "Mr Test")

        playerColor = PlayerColor.Blue
        playerID = "Mr " + str(playerColor).capitalize()
        self.gameManager.AddPlayerToGame(gameID, playerID)

        playerColor = PlayerColor.Red
        playerID = "Mr " + str(playerColor).capitalize()
        self.gameManager.AddPlayerToGame(gameID, playerID)

        success = self.gameManager.StartGame(gameID)[0]
        self.assertTrue(success)

    def testStartGame_InvalidID(self):
        gameID = 123456789

        success = self.gameManager.StartGame(gameID)[0]
        self.assertFalse(success)

    def testDoPlayerAction(self):
        gameID = self.gameManager.CreateGame("TestGame", "Mr Test")

        playerColor = PlayerColor.Blue
        playerID = "Mr " + str(playerColor).capitalize()
        self.gameManager.AddPlayerToGame(gameID, playerID)

        playerColor = PlayerColor.Red
        playerID = "Mr " + str(playerColor).capitalize()
        self.gameManager.AddPlayerToGame(gameID, playerID)

        self.gameManager.StartGame(gameID)

        game = self.gameManager.GetGameByID(gameID)

        success = self.gameManager.DoPlayerAction(gameID, game.currentPlayer.GetID(), ResupplyAction(0, 0))[0]
        self.assertTrue(success)

    def testDoPlayerAction_InvalidGameID(self):
        gameID = 123456789

        playerColor = PlayerColor.Blue
        playerID = "Mr " + str(playerColor).capitalize()

        success = self.gameManager.DoPlayerAction(gameID, playerID, ResupplyAction(0, 0))[0]
        self.assertFalse(success)

def TheTestSuite():
    return unittest.defaultTestLoader.loadTestsFromTestCase(GameManagerTestCase)

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(TheTestSuite())

if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from hansa.GameManager import GameManager
from hansa.Game import GameStatus
from hansa.Player import PlayerColor

import unittest

class GameTestCase(unittest.TestCase):
    def setUp(self):
        self.gameManager = GameManager(True)
        id = self.gameManager.CreateGame("TestGame", "Mr Test")
        self.game = self.gameManager.GetGameByID(id)
        self.playerColors = PlayerColor().GetAll()

    def tearDown(self):
        self.gameManager.Shutdown()

    def testInitialGameState(self):
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForMorePlayers)
        self.assertEqual(len(self.game.players), 0)

    def testAddPlayer(self):
        playerColor = self.playerColors[0]
        playerName = "Mr " + str(playerColor).capitalize()
        self.game.AddPlayer(playerName, str(playerColor))

        self.assertEqual(len(self.game.players), 1)
        self.assertEqual([p.GetName() for p in self.game.players].count(playerName), 1)

        player = next(p for p in self.game.players if p.GetName() == playerName)
        self.assertEqual(player.GetColor(), playerColor)

    def testAddPlayer_UnspecifiedColor(self):
        playerName = "Mr Unspecified"
        self.game.AddPlayer(playerName)

        self.assertEqual(len(self.game.players), 1)
        self.assertEqual([p.GetName() for p in self.game.players].count(playerName), 1)

        player = next(p for p in self.game.players if p.GetName() == playerName)
        self.assertTrue(player.GetColor() in self.playerColors)

    def testAddPlayer_InvalidColor(self):
        colorName = "invalid color"
        playerName = "Mr " + str(colorName).capitalize()
        self.game.AddPlayer(playerName, colorName)

        self.assertEqual(len(self.game.players), 1)
        self.assertEqual([p.GetName() for p in self.game.players].count(playerName), 1)

        player = next(p for p in self.game.players if p.GetName() == playerName)
        self.assertTrue(player.GetColor() in self.playerColors)

    def testAddPlayer_AlreadyExists(self):
        playerColor = self.playerColors[0]
        playerName = "Mr " + str(playerColor).capitalize()
        self.game.AddPlayer(playerName, str(playerColor))
        self.game.AddPlayer(playerName, str(playerColor))

        self.assertEqual(len(self.game.players), 1)
        self.assertEqual([p.GetName() for p in self.game.players].count(playerName), 1)

    def testAddPlayer_TooManyPlayers(self):
        for playerColor in self.playerColors:
            playerName = "Mr " + str(playerColor).capitalize()
            self.game.AddPlayer(playerName, str(playerColor))

        playerName = "Mr One Too Many"
        self.game.AddPlayer(playerName)

        self.assertEqual(len(self.game.players), len(self.playerColors))
        self.assertEqual([p.GetName() for p in self.game.players].count(playerName), 0)

    def testAddPlayer_GameAlreadyStarted(self):
        for playerColor in self.playerColors[:2]: # need at least 2 players to start a game
            playerName = "Mr " + str(playerColor).capitalize()
            self.game.AddPlayer(playerName, str(playerColor))

        self.game.Start()

        playerName = "Mr Too Late"
        self.game.AddPlayer(playerName)

        self.assertEqual(len(self.game.players), 2)
        self.assertEqual([p.GetName() for p in self.game.players].count(playerName), 0)

    def testStart(self):
        for playerColor in self.playerColors:
            playerName = "Mr " + str(playerColor).capitalize()
            self.game.AddPlayer(playerName, str(playerColor))

        self.assertEqual(self.game.GetState().status, GameStatus.ReadyToStart)

        self.game.Start()

        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForCurrentPlayer)
        self.assertTrue(self.game.GetState().currentPlayer.HasActions())
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

    def testStart_NoPlayers(self):
        self.game.Start()

        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForMorePlayers)
        self.assertTrue(self.game.GetState().currentPlayer is None)

    def testStart_OnePlayer(self):
        playerName = "Mr Test"
        self.game.AddPlayer(playerName)

        self.game.Start()

        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForMorePlayers)
        self.assertTrue(self.game.GetState().currentPlayer is None)

    def testStart_AlreadyStarted(self):
        for playerColor in self.playerColors:
            playerName = "Mr " + str(playerColor).capitalize()
            self.game.AddPlayer(playerName, str(playerColor))

        self.assertEqual(self.game.GetState().status, GameStatus.ReadyToStart)

        success = self.game.Start()[0]
        self.assertTrue(success)

        success = self.game.Start()[0]
        self.assertFalse(success)

    def testStop(self):
        for playerColor in self.playerColors:
            playerName = "Mr " + str(playerColor).capitalize()
            self.game.AddPlayer(playerName, str(playerColor))

        self.game.Start()
        self.game.Stop()

        self.assertEqual(self.game.GetState().status, GameStatus.GameOver)

def TheTestSuite():
    return unittest.defaultTestLoader.loadTestsFromTestCase(GameTestCase)

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(TheTestSuite())

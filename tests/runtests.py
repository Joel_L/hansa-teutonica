if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import unittest

import testGameManager
import testGame
import testSetup
import testPlayerSkills
import testActions
import testBonusMarkers

if __name__ == '__main__':
    allTests = [
        testGameManager.TheTestSuite(),
        testGame.TheTestSuite(),
        testSetup.TheTestSuite(),
        testPlayerSkills.TheTestSuite(),
        testActions.TheTestSuite(),
        testBonusMarkers.TheTestSuite()
    ]

    allTestsSuite = unittest.TestSuite(allTests)
    unittest.TextTestRunner(verbosity=2).run(allTestsSuite)

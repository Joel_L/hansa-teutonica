if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from hansa.Game import GameStatus
from hansa.GameManager import GameManager
from hansa.Skill import SkillTypes
from hansa.Resource import ResourceType
from hansa.Action import ResupplyAction, PlaceResourceAction, DisplaceResourceAction, MoveResourcesAction, EstablishTradeRouteAction, Action
from hansa.Action import PlaceDisplacedResourceReaction, DisplacedResourceMovement

import unittest

class ActionsTestCase(unittest.TestCase):
    def setUp(self):
        self.gameManager = GameManager(True)
        id = self.gameManager.CreateGame("TestGame", "Mr Test")
        self.game = self.gameManager.GetGameByID(id)
        self.game.AddPlayer("Player One")
        self.game.AddPlayer("Player Two")
        self.game.Start()

    def tearDown(self):
        self.gameManager.Shutdown()

    def testAction_InvalidPlayer(self):
        success = self.game.DoPlayerAction("123abc", ResupplyAction(0, 0))[0]
        self.assertFalse(success)

    def testAction_InvalidAction(self):
        success = self.game.DoPlayerAction(self.game.currentPlayer.GetID(), Action())[0]
        self.assertFalse(success)

    def testResupplyAction(self):
        numTradersBefore = self.game.currentPlayer.personalSupplyResourcePool.GetNumTraders()

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(1, 0))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, 1)

        numTradersAfter = self.game.currentPlayer.personalSupplyResourcePool.GetNumTraders()
        numTradersAdded = numTradersAfter - numTradersBefore
        self.assertEqual(numTradersAdded, 1)

    def testResupplyAction_NotSkilledEnough(self):
        numTradersBefore = self.game.currentPlayer.personalSupplyResourcePool.GetNumTraders()

        numAllowedResupply = self.game.currentPlayer._GetSkill(SkillTypes.Money).GetValue()
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(int(numAllowedResupply / 2) + 1, int(numAllowedResupply / 2) + 1))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, 2)

        numTradersAfter = self.game.currentPlayer.personalSupplyResourcePool.GetNumTraders()
        self.assertEqual(numTradersBefore, numTradersAfter)

    def testResupplyAction_NotEnoughTraders(self):
        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Money)
        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Money)
        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Money)

        numTradersBefore = self.game.currentPlayer.personalSupplyResourcePool.GetNumTraders()

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(7, 0))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, 2)

        numTradersAfter = self.game.currentPlayer.personalSupplyResourcePool.GetNumTraders()
        self.assertEqual(numTradersBefore, numTradersAfter)

    def testResupplyAction_NotEnoughMerchants(self):
        numMerchantsBefore = self.game.currentPlayer.personalSupplyResourcePool.GetNumMerchants()

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 1))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, 2)

        numMerchantsAfter = self.game.currentPlayer.personalSupplyResourcePool.GetNumMerchants()
        self.assertEqual(numMerchantsBefore, numMerchantsAfter)

    def testResupplyAction_NegativeAmounts(self):
        numMerchantsBefore = self.game.currentPlayer.personalSupplyResourcePool.GetNumMerchants()

        self.assertRaises(ValueError, self.game.DoPlayerAction, self.game.currentPlayer.GetID(), ResupplyAction(-1, -1))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, 2)

        numMerchantsAfter = self.game.currentPlayer.personalSupplyResourcePool.GetNumMerchants()
        self.assertEqual(numMerchantsBefore, numMerchantsAfter)

    def testPlaceResourceAction(self):
        fieldID = "1-2.1"
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

        field = self.game.board.GetFieldByID(fieldID)
        self.assertTrue(field.IsOccupiedBy(self.game.currentPlayer))
        self.assertEqual(field.GetOccupyingResource().GetType(), resourceType)

    def testPlaceResourceAction_DestinationIsOccupied(self):
        fieldID = "1-2.1"
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldID))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

    def testPlaceResourceAction_NotEnoughResources(self):
        fieldID = "1-2.1"
        resourceType = ResourceType.Merchant
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldID))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

    def testDisplaceResourceAction_Trader(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))

        playerIDs.append(self.game.currentPlayer.GetID())
        self.assertNotEqual(playerIDs[0], playerIDs[1])

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Merchant
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForPlayerReaction)

        field = self.game.board.GetFieldByID(fieldIDs[0])
        self.assertTrue(field.IsOccupied())
        self.assertEqual(field.GetOccupyingResource().GetOwnerPlayerID(), playerIDs[1])
        self.assertEqual(field.GetOccupyingResource().GetType(), resourceType)

    def testDisplaceResourceAction_Merchant(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Merchant, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))

        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 1
        numMerchantsPenalty = 1
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForPlayerReaction)

        field = self.game.board.GetFieldByID(fieldIDs[0])
        self.assertTrue(field.IsOccupied())
        self.assertEqual(field.GetOccupyingResource().GetOwnerPlayerID(), playerIDs[1])
        self.assertEqual(field.GetOccupyingResource().GetType(), resourceType)

    def testDisplaceResourceAction_NothingToDisplace(self):
        fieldID = "1-2.1"
        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

        field = self.game.board.GetFieldByID(fieldID)
        self.assertFalse(field.IsOccupied())

    def testDisplaceResourceAction_NotEnoughResources(self):
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Merchant, fieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))

        numTradersPenalty = 0
        numMerchantsPenalty = 2
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

        field = self.game.board.GetFieldByID(fieldIDs[0])
        self.assertNotEqual(field.GetOccupyingResource().GetOwnerPlayerID(), self.game.currentPlayer.GetID())

    def testDisplaceResourceAction_PenaltyNotPaid(self):
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Merchant, fieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

        field = self.game.board.GetFieldByID(fieldIDs[0])
        self.assertNotEqual(field.GetOccupyingResource().GetOwnerPlayerID(), self.game.currentPlayer.GetID())

    def testDisplaceResourceAction_InvalidFieldID(self):
        fieldID = "123abc"
        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

    def testDisplaceResourceAction_AlreadyOccupiedBySamePlayer(self):
        fieldID = "1-2.1"
        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Trader, fieldID))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

    def testPlaceDisplacedResourceReaction_Trader(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Merchant
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["2-3.1", "2-3.2"]
        destinationResourceTypes = [ResourceType.Trader, ResourceType.Trader]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1])]

        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
        self.assertEqual(numTradersBefore - numTradersAfter, 1)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForCurrentPlayer)

        for i, fieldID in enumerate(destinationFieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())
            self.assertEqual(field.GetOccupyingResource().GetOwnerPlayerID(), playerIDs[0])
            self.assertEqual(field.GetOccupyingResource().GetType(), destinationResourceTypes[i])

    def testPlaceDisplacedResourceReaction_Merchant(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Merchant, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 2
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["2-3.1", "2-3.2", "2-3.3"]
        destinationResourceTypes = [ResourceType.Merchant, ResourceType.Trader, ResourceType.Trader]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1]),
            DisplacedResourceMovement(destinationResourceTypes[2], destinationFieldIDs[2])]

        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
        self.assertEqual(numTradersBefore - numTradersAfter, 2)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForCurrentPlayer)

        for i, fieldID in enumerate(destinationFieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())
            self.assertEqual(field.GetOccupyingResource().GetOwnerPlayerID(), playerIDs[0])
            self.assertEqual(field.GetOccupyingResource().GetType(), destinationResourceTypes[i])

    def testPlaceDisplacedResourceReaction_NoExtraResources(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Merchant
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["2-3.1"]
        destinationResourceTypes = [ResourceType.Trader]
        resourceMovements = [DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0])]

        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
        self.assertEqual(numTradersBefore - numTradersAfter, 0)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForCurrentPlayer)

        for i, fieldID in enumerate(destinationFieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())
            self.assertEqual(field.GetOccupyingResource().GetOwnerPlayerID(), playerIDs[0])
            self.assertEqual(field.GetOccupyingResource().GetType(), destinationResourceTypes[i])

    def testPlaceDisplacedResourceReaction_TooManyExtraResources(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Merchant
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["2-3.1", "2-3.2", "2-3.3"]
        destinationResourceTypes = [ResourceType.Trader, ResourceType.Trader, ResourceType.Trader]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1]),
            DisplacedResourceMovement(destinationResourceTypes[2], destinationFieldIDs[2])]

        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
        self.assertEqual(numTradersBefore - numTradersAfter, 0)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForPlayerReaction)

    def testPlaceDisplacedResourceReaction_MissingResourceButStockIsNotEmpty(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Merchant
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["2-3.1", "2-3.2"]
        destinationResourceTypes = [ResourceType.Trader, ResourceType.Merchant]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1])]

        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.assertEqual(self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumMerchants(), 0)
        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.assertEqual(self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumMerchants(), 0)
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
        self.assertEqual(numTradersBefore - numTradersAfter, 0)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForPlayerReaction)

    def testPlaceDisplacedResourceReaction_StockIsEmpty(self):
        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Money)
        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Money)
        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Money)

        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(6, 0))
        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["2-3.1", "2-3.2"]
        destinationResourceTypes = [ResourceType.Trader, ResourceType.Trader]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1])]

        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.GetNumTraders()
        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.GetNumTraders()
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
        self.assertEqual(numTradersBefore - numTradersAfter, 1)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForCurrentPlayer)

    # def testPlaceDisplacedResourceReaction_MissingResourceButSupplyIsNotEmpty(self):
    #     for _ in range(3):
    #         self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Money)

    #     playerIDs = [self.game.currentPlayer.GetID()]
    #     fieldIDs = ["1-2.1"]
    #     self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Merchant, fieldIDs[0]))
    #     self.game.DoPlayerAction(playerIDs[0], ResupplyAction(6, 0))
    #     playerIDs.append(self.game.currentPlayer.GetID())

    #     numTradersPenalty = 2
    #     numMerchantsPenalty = 0
    #     resourceType = ResourceType.Trader
    #     self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

    #     destinationFieldIDs = ["2-3.1", "2-3.2", "2-3.3"]
    #     destinationResourceTypes = [ResourceType.Trader, ResourceType.Merchant, ResourceType.Merchant]
    #     resourceMovements = [
    #         DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
    #         DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1]),
    #         DisplacedResourceMovement(destinationResourceTypes[2], destinationFieldIDs[2])]

    #     numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.GetNumTraders()
    #     self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
    #     numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.GetNumTraders()
    #     self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
    #     self.assertEqual(numTradersBefore - numTradersAfter, 0)
    #     self.assertEqual(self.game.GetState().status, GameStatus.WaitingForPlayerReaction)

    def testPlaceDisplacedResourceReaction_StockAndSupplyEmpty_NoSourceFieldSpecified(self):
        for _ in range(3):
            self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Money)

        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Merchant, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(6, 0))
        playerIDs.append(self.game.currentPlayer.GetID())

        self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.numTraders = 0
        self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.numMerchants = 0

        numTradersPenalty = 2
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["2-3.1", "2-3.2", "2-3.3"]
        destinationResourceTypes = [ResourceType.Trader, ResourceType.Merchant, ResourceType.Merchant]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1]),
            DisplacedResourceMovement(destinationResourceTypes[2], destinationFieldIDs[2])]

        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.GetNumTraders()
        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.GetNumTraders()
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
        self.assertEqual(numTradersBefore - numTradersAfter, 0)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForPlayerReaction)

    def testPlaceDisplacedResourceReaction_StockIsEmptyThenSupply_AdjacentRoutesAreFull(self):
        for _ in range(3):
            self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Money)

        playerIDs = [self.game.currentPlayer.GetID()]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Merchant, "1-2.1"))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "1-2.2"))
        playerIDs.append(self.game.currentPlayer.GetID())

        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "1-2.3"))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "2-3.1"))

        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "2-3.2"))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "2-3.3"))

        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(6, 0))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "2-3.4"))

        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "3-4.1"))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "3-4.2"))

        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "3-4.3"))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "3-15.1"))

        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "3-15.2"))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "15-16.1"))

        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "15-16.2"))
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))

        self.assertEqual(self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.GetNumTraders(), 1)

        numTradersPenalty = 2
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, "1-2.1"))

        destinationFieldIDs = ["15-16.3", "4-5.2", "4-13.3"]
        destinationResourceTypes = [ResourceType.Merchant, ResourceType.Trader, ResourceType.Trader]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1]),
            DisplacedResourceMovement(destinationResourceTypes[2], destinationFieldIDs[2], "1-2.2")] # second extra piece comes from the board

        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
        self.assertEqual(self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerIDs[0]).personalSupplyResourcePool.GetNumTraders(), 0)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForCurrentPlayer)

        field = self.game.board.GetFieldByID("1-2.2")
        self.assertFalse(field.IsOccupied())

        for i, fieldID in enumerate(destinationFieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())
            self.assertEqual(field.GetOccupyingResource().GetOwnerPlayerID(), playerIDs[0])
            self.assertEqual(field.GetOccupyingResource().GetType(), destinationResourceTypes[i])

    def testPlaceDisplacedResourceReaction_TooFarAway(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Merchant
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["3-15.1", "3-15.2"]
        destinationResourceTypes = [ResourceType.Trader, ResourceType.Trader]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1])]

        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForPlayerReaction)

        for i, fieldID in enumerate(destinationFieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertFalse(field.IsOccupied())

    def testPlaceDisplacedResourceReaction_AdjacentRouteIsFull(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
        playerIDs.append(self.game.currentPlayer.GetID())

        self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, "1-2.3"))
        self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, "2-3.1"))

        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "2-3.2"))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "2-3.3"))

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Merchant
        self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, "2-3.4"))
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["3-15.1", "3-15.2"]
        destinationResourceTypes = [ResourceType.Trader, ResourceType.Trader]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1])]

        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[0])
        self.assertEqual(numTradersBefore - numTradersAfter, 1)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForCurrentPlayer)

        for i, fieldID in enumerate(destinationFieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())
            self.assertEqual(field.GetOccupyingResource().GetOwnerPlayerID(), playerIDs[0])
            self.assertEqual(field.GetOccupyingResource().GetType(), destinationResourceTypes[i])

    # def testPlaceDisplacedResourceReaction_DestinationIsOccupied(self):
    #     playerIDs = [self.game.currentPlayer.GetID()]
    #     fieldIDs = ["1-2.1", "1-2.2"]
    #     self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
    #     self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
    #     playerIDs.append(self.game.currentPlayer.GetID())

    #     self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, "1-2.3"))
    #     self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, "2-3.1"))

    #     self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "2-3.2"))
    #     self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, "2-3.3"))

    #     numTradersPenalty = 1
    #     numMerchantsPenalty = 0
    #     resourceType = ResourceType.Merchant
    #     self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

    #     destinationFieldIDs = ["2-3.3", "2-3.4"]
    #     destinationResourceTypes = [ResourceType.Trader, ResourceType.Trader]
    #     resourceMovements = [
    #         DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
    #         DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1])]

    #     numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
    #     self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
    #     numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
    #     self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
    #     self.assertEqual(numTradersBefore - numTradersAfter, 0)
    #     self.assertEqual(self.game.GetState().status, GameStatus.WaitingForPlayerReaction)

    def testPlaceDisplacedResourceReaction_InvalidFieldID(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))

        destinationFieldIDs = ["123abc", "2-3.4"]
        destinationResourceTypes = [ResourceType.Trader, ResourceType.Trader]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1])]

        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.game.DoPlayerAction(playerIDs[0], PlaceDisplacedResourceReaction(resourceMovements))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.assertEqual(self.game.currentPlayer.GetID(), playerIDs[1])
        self.assertEqual(numTradersBefore - numTradersAfter, 0)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForPlayerReaction)

    def testPlaceDisplacedResourceReaction_WrongAction(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        fieldIDs = ["1-2.1", "1-2.2"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, fieldIDs[1]))
        playerIDs.append(self.game.currentPlayer.GetID())

        numTradersPenalty = 1
        numMerchantsPenalty = 0
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(playerIDs[1], DisplaceResourceAction(resourceType, numTradersPenalty, numMerchantsPenalty, fieldIDs[0]))
        numTradersBefore = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(1, 0))
        numTradersAfter = self.game._GetPlayerByID(playerIDs[0]).stockResourcePool.GetNumTraders()
        self.assertEqual(numTradersBefore - numTradersAfter, 0)

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

    def testPlaceDisplacedResourceReaction_NothingWasDisplaced(self):
        destinationFieldIDs = ["2-3.1", "2-3.2", "2-3.3"]
        destinationResourceTypes = [ResourceType.Trader, ResourceType.Trader, ResourceType.Trader]
        resourceMovements = [
            DisplacedResourceMovement(destinationResourceTypes[0], destinationFieldIDs[0]),
            DisplacedResourceMovement(destinationResourceTypes[1], destinationFieldIDs[1]),
            DisplacedResourceMovement(destinationResourceTypes[2], destinationFieldIDs[2])]

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceDisplacedResourceReaction(resourceMovements))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)
        self.assertEqual(self.game.GetState().status, GameStatus.WaitingForCurrentPlayer)

    def testMoveResourcesAction(self):
        sourceFieldID = "1-2.1"
        targetFieldID = "1-2.2"
        resourceType = ResourceType.Trader
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, sourceFieldID))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), MoveResourcesAction([sourceFieldID], [targetFieldID]))

        sourceField = self.game.board.GetFieldByID(sourceFieldID)
        self.assertFalse(sourceField.IsOccupied())

        targetField = self.game.board.GetFieldByID(targetFieldID)
        self.assertTrue(targetField.IsOccupied())

    def testMoveResourcesAction_MultipleResources(self):
        sourceFieldIDs = ["1-2.1", "1-2.2"]
        targetFieldIDs = ["1-2.3", "2-3.1"]
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Trader, sourceFieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Merchant, sourceFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), MoveResourcesAction(sourceFieldIDs, targetFieldIDs))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

        for sourceFieldID in sourceFieldIDs:
            sourceField = self.game.board.GetFieldByID(sourceFieldID)
            self.assertFalse(sourceField.IsOccupied())

        for targetFieldID in targetFieldIDs:
            targetField = self.game.board.GetFieldByID(targetFieldID)
            self.assertTrue(targetField.IsOccupied())

    def testMoveResourcesAction_NotSkilledEnough(self):
        sourceFieldIDs = ["1-2.1", "1-2.2", "1-2.3"]
        targetFieldIDs = ["2-3.1", "2-3.2", "2-3.3"]
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Trader, sourceFieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Merchant, sourceFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Trader, sourceFieldIDs[2]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), MoveResourcesAction(sourceFieldIDs, targetFieldIDs))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

        for sourceFieldID in sourceFieldIDs:
            sourceField = self.game.board.GetFieldByID(sourceFieldID)
            self.assertTrue(sourceField.IsOccupied())

        for targetFieldID in targetFieldIDs:
            targetField = self.game.board.GetFieldByID(targetFieldID)
            self.assertFalse(targetField.IsOccupied())

    def testMoveResourcesAction_MismatchedFieldIDs(self):
        sourceFieldIDs = ["1-2.1"]
        targetFieldIDs = ["2-3.1", "2-3.2"]
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Trader, sourceFieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), MoveResourcesAction(sourceFieldIDs, targetFieldIDs))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

    def testMoveResourcesAction_AlreadyOccupied(self):
        sourceFieldIDs = ["1-2.1"]
        targetFieldIDs = ["1-2.1"]
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Trader, sourceFieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), MoveResourcesAction(sourceFieldIDs, targetFieldIDs))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

    def testMoveResourcesAction_NothingToMove(self):
        sourceFieldIDs = ["1-2.1"]
        targetFieldIDs = ["1-2.1"]
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), MoveResourcesAction(sourceFieldIDs, targetFieldIDs))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

    def testMoveResourcesAction_InvalidFieldID(self):
        validFieldID = "1-2.1"
        invalidFieldID = "123abc"
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), MoveResourcesAction([invalidFieldID], [validFieldID]))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(ResourceType.Trader, validFieldID))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), MoveResourcesAction([validFieldID], [invalidFieldID]))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

    def testEstablishTradeRouteAction_WithBonusPoint(self):
        playerID = self.game.currentPlayer.GetID()
        cityID = "1"
        routeFieldIDs = ["1-2.1", "1-2.2", "1-2.3"]
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Merchant, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[2]))
        self.game.DoPlayerAction(playerID, EstablishTradeRouteAction(routeFieldIDs[0], cityID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

        for routeFieldID in routeFieldIDs:
            routeField = self.game.board.GetFieldByID(routeFieldID)
            self.assertFalse(routeField.IsOccupied())

        city = self.game.board.GetCityByID(cityID)
        office = city.GetOfficeByIndex(0)
        self.assertEqual(office.GetOccupyingPlayerID(), playerID)

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerID).GetPoints(), 1)

    def testEstablishTradeRouteAction_WithBonusMarker(self):
        playerID = self.game.currentPlayer.GetID()
        cityID = "3"
        routeFieldIDs = ["3-4.1", "3-4.2", "3-4.3"]

        route = self.game.board.GetRouteByFieldID(routeFieldIDs[0])
        self.assertTrue(route.HasBonusMarker())
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 0)

        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[2]))
        self.game.DoPlayerAction(playerID, EstablishTradeRouteAction(routeFieldIDs[0], cityID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)

        self.assertFalse(route.HasBonusMarker())
        self.assertNotEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 0)

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerID).GetPoints(), 0)

    def testEstablishTradeRouteAction_WithMerchant(self):
        playerID = self.game.currentPlayer.GetID()
        cityID = "2"
        routeFieldIDs = ["1-2.1", "1-2.2", "1-2.3"]
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Merchant, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[2]))
        self.game.DoPlayerAction(playerID, EstablishTradeRouteAction(routeFieldIDs[0], cityID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

        for routeFieldID in routeFieldIDs:
            routeField = self.game.board.GetFieldByID(routeFieldID)
            self.assertFalse(routeField.IsOccupied())

        city = self.game.board.GetCityByID(cityID)
        office = city.GetOfficeByIndex(0)
        self.assertEqual(office.GetOccupyingPlayerID(), playerID)

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerID).GetPoints(), 0)

    def testEstablishTradeRouteAction_WrongResourceTypeForOffice(self):
        playerID = self.game.currentPlayer.GetID()
        cityID = "2"
        routeFieldIDs = ["1-2.1", "1-2.2", "1-2.3"]
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[2]))
        self.game.DoPlayerAction(playerID, EstablishTradeRouteAction(routeFieldIDs[0], cityID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

        for routeFieldID in routeFieldIDs:
            routeField = self.game.board.GetFieldByID(routeFieldID)
            self.assertTrue(routeField.IsOccupied())

        city = self.game.board.GetCityByID(cityID)
        self.assertFalse(city.HasOfficeOccupiedByPlayer(self.game.GetState().currentPlayer))

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerID).GetPoints(), 0)

    def testEstablishTradeRouteAction_WithPrivilege(self):
        playerID = self.game.currentPlayer.GetID()
        cityID = "15"
        routeFieldIDs = ["3-15.1", "3-15.2"]
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Privilege)
        self.game.DoPlayerAction(playerID, EstablishTradeRouteAction(routeFieldIDs[0], cityID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

        for routeFieldID in routeFieldIDs:
            routeField = self.game.board.GetFieldByID(routeFieldID)
            self.assertFalse(routeField.IsOccupied())

        city = self.game.board.GetCityByID(cityID)
        self.assertTrue(city.HasOfficeOccupiedByPlayer(self.game.GetState().currentPlayer))

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerID).GetPoints(), 0)

    def testEstablishTradeRouteAction_WrongPrivilegeForOffice(self):
        playerID = self.game.currentPlayer.GetID()
        cityID = "15"
        routeFieldIDs = ["3-15.1", "3-15.2"]
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerID, EstablishTradeRouteAction(routeFieldIDs[0], cityID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

        for routeFieldID in routeFieldIDs:
            routeField = self.game.board.GetFieldByID(routeFieldID)
            self.assertTrue(routeField.IsOccupied())

        city = self.game.board.GetCityByID(cityID)
        self.assertFalse(city.HasOfficeOccupiedByPlayer(self.game.GetState().currentPlayer))

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerID).GetPoints(), 0)

    def testEstablishTradeRouteAction_RouteNotFull(self):
        cityID = "15"
        routeFieldID = "1-2.1"

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), EstablishTradeRouteAction(routeFieldID, cityID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

        city = self.game.board.GetCityByID(cityID)
        self.assertFalse(city.HasOfficeOccupiedByPlayer(self.game.GetState().currentPlayer))

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)

    def testEstablishTradeRouteAction_InvalidFieldID(self):
        cityID = "15"
        invalidFieldID = "123abc"

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), EstablishTradeRouteAction(invalidFieldID, cityID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

        city = self.game.board.GetCityByID(cityID)
        self.assertFalse(city.HasOfficeOccupiedByPlayer(self.game.GetState().currentPlayer))

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)

    def testEstablishTradeRouteAction_CityNotOnRoute(self):
        playerID = self.game.currentPlayer.GetID()
        cityID = "16"
        routeFieldIDs = ["3-15.1", "3-15.2"]
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerID, EstablishTradeRouteAction(routeFieldIDs[0], cityID))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)

        for routeFieldID in routeFieldIDs:
            routeField = self.game.board.GetFieldByID(routeFieldID)
            self.assertTrue(routeField.IsOccupied())

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerID).GetPoints(), 0)

    def testEstablishTradeRouteAction_UpgradeSkill(self):
        playerID = self.game.currentPlayer.GetID()
        cityID = "25"
        routeFieldIDs = ["23-25.1", "23-25.2", "23-25.3"]
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[2]))
        self.game.DoPlayerAction(playerID, EstablishTradeRouteAction(routeFieldIDs[0], cityID, SkillTypes.Actions))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

        for routeFieldID in routeFieldIDs:
            routeField = self.game.board.GetFieldByID(routeFieldID)
            self.assertFalse(routeField.IsOccupied())

        city = self.game.board.GetCityByID(cityID)
        self.assertFalse(city.HasOfficeOccupiedByPlayer(self.game._GetPlayerByID(playerID)))

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerID).GetPoints(), 0)

    def testEstablishTradeRouteAction_WrongSkillForCity(self):
        playerID = self.game.currentPlayer.GetID()
        cityID = "25"
        routeFieldIDs = ["23-25.1", "23-25.2", "23-25.3"]
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can continue the test.
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, routeFieldIDs[2]))
        self.game.DoPlayerAction(playerID, EstablishTradeRouteAction(routeFieldIDs[0], cityID, SkillTypes.Money))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 1)

        for routeFieldID in routeFieldIDs:
            routeField = self.game.board.GetFieldByID(routeFieldID)
            self.assertTrue(routeField.IsOccupied())

        city = self.game.board.GetCityByID(cityID)
        self.assertFalse(city.HasOfficeOccupiedByPlayer(self.game._GetPlayerByID(playerID)))

        self.assertEqual(self.game.currentPlayer.GetPoints(), 0)
        self.assertEqual(self.game._GetPlayerByID(playerID).GetPoints(), 0)

    def testEstablishTradeRouteAction_WithControllingPlayer(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        cityID = "19"
        routeFieldIDs = ["19-20.1", "19-20.2", "19-20.3"]
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, routeFieldIDs[1]))

        # Skip the second player's turn so the first player can establish a route and get a controlling office.
        playerIDs.append(self.game.currentPlayer.GetID())
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerIDs[0], PlaceResourceAction(ResourceType.Trader, routeFieldIDs[2]))
        self.game.DoPlayerAction(playerIDs[0], EstablishTradeRouteAction(routeFieldIDs[0], cityID))

        city = self.game.board.GetCityByID(cityID)
        self.assertEqual(city.GetControllingPlayerID(), playerIDs[0])

        # Now the second player starts working on the same route.
        self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Merchant, routeFieldIDs[1]))

        # Skip the first player's turn.
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))

        # Second player claims the route which should award a point to the first player (since they have control)
        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Privilege)
        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Privilege)
        self.game.GetState().currentPlayer.UpgradeSkill(SkillTypes.Privilege)
        self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, routeFieldIDs[2]))
        self.game.DoPlayerAction(playerIDs[1], EstablishTradeRouteAction(routeFieldIDs[0], cityID))

        self.assertEqual(self.game._GetPlayerByID(playerIDs[0]).GetPoints(), 1)
        self.assertEqual(self.game._GetPlayerByID(playerIDs[1]).GetPoints(), 0)

        # Controlling player changed because player 2 now has the right-most office in the city.
        self.assertEqual(city.GetControllingPlayerID(), playerIDs[1])

        # The city is considered "complete".
        self.assertEqual(self.game.numCompletedCities, 1)

        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))

        # Second player again wants to claim the route
        self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, routeFieldIDs[0]))
        self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, routeFieldIDs[1]))

        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))

        self.game.DoPlayerAction(playerIDs[1], PlaceResourceAction(ResourceType.Trader, routeFieldIDs[2]))
        self.game.DoPlayerAction(playerIDs[1], EstablishTradeRouteAction(routeFieldIDs[0], cityID))

        # ...but now they can't because all the offices are occupied.
        self.assertEqual(self.game._GetPlayerByID(playerIDs[1]).numActionsRemaining, 1)

def TheTestSuite():
    return unittest.defaultTestLoader.loadTestsFromTestCase(ActionsTestCase)

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(TheTestSuite())

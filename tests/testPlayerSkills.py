if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from hansa.GameManager import GameManager
from hansa.Player import Player, PlayerColor
from hansa.Skill import SkillTypes

import sys
import unittest

class PlayerSkillsTestCase(unittest.TestCase):
    def setUp(self):
        self.gameManager = GameManager(True)
        self.player = self.CreatePlayer("Mr Red", PlayerColor.Red)
        self.player.Init(0)
        self.player2 = self.CreatePlayer("Mr Blue", PlayerColor.Blue)
        self.player2.Init(1)
        self.startingNumTraders = self.player.personalSupplyResourcePool.GetNumTraders()
        self.startingNumMerchants = self.player.personalSupplyResourcePool.GetNumMerchants()

    def tearDown(self):
        self.gameManager.Shutdown()

    def testTownKeySkill(self):
        self.assertEqual(self.player.GetTownKeyValue(), 1)
        self.player.UpgradeSkill(SkillTypes.TownKey)
        self.assertEqual(self.player.GetTownKeyValue(), 2)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 1)
        self.player.UpgradeSkill(SkillTypes.TownKey)
        self.assertEqual(self.player.GetTownKeyValue(), 2)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 2)
        self.player.UpgradeSkill(SkillTypes.TownKey)
        self.assertEqual(self.player.GetTownKeyValue(), 3)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 3)
        self.player.UpgradeSkill(SkillTypes.TownKey)
        self.assertEqual(self.player.GetTownKeyValue(), 4)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 4)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumMerchants(), self.startingNumMerchants)
        self.assertFalse(self.player.UpgradeSkill(SkillTypes.TownKey)[0])

    def testActionSkill(self):
        self.assertEqual(self.player._GetSkill(SkillTypes.Actions).GetValue(), 2)
        self.player.UpgradeSkill(SkillTypes.Actions)
        self.assertEqual(self.player._GetSkill(SkillTypes.Actions).GetValue(), 3)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 1)
        self.player.UpgradeSkill(SkillTypes.Actions)
        self.assertEqual(self.player._GetSkill(SkillTypes.Actions).GetValue(), 3)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 2)
        self.player.UpgradeSkill(SkillTypes.Actions)
        self.assertEqual(self.player._GetSkill(SkillTypes.Actions).GetValue(), 4)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 3)
        self.player.UpgradeSkill(SkillTypes.Actions)
        self.assertEqual(self.player._GetSkill(SkillTypes.Actions).GetValue(), 4)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 4)
        self.player.UpgradeSkill(SkillTypes.Actions)
        self.assertEqual(self.player._GetSkill(SkillTypes.Actions).GetValue(), 5)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 5)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumMerchants(), self.startingNumMerchants)
        self.assertFalse(self.player.UpgradeSkill(SkillTypes.Actions)[0])

    def testPrivilegeSkill(self):
        self.assertTrue(self.player.HasPrivilegeLevel(1))
        self.player.UpgradeSkill(SkillTypes.Privilege)
        self.assertTrue(self.player.HasPrivilegeLevel(2))
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 1)
        self.player.UpgradeSkill(SkillTypes.Privilege)
        self.assertTrue(self.player.HasPrivilegeLevel(3))
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 2)
        self.player.UpgradeSkill(SkillTypes.Privilege)
        self.assertTrue(self.player.HasPrivilegeLevel(4))
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 3)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumMerchants(), self.startingNumMerchants)
        self.assertFalse(self.player.UpgradeSkill(SkillTypes.Privilege)[0])

    def testBookSkill(self):
        self.assertEqual(self.player.GetMovementAllowance(), 2)
        self.player.UpgradeSkill(SkillTypes.BookOfLore)
        self.assertEqual(self.player.GetMovementAllowance(), 3)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumMerchants(), self.startingNumMerchants + 1)
        self.player.UpgradeSkill(SkillTypes.BookOfLore)
        self.assertEqual(self.player.GetMovementAllowance(), 4)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumMerchants(), self.startingNumMerchants + 2)
        self.player.UpgradeSkill(SkillTypes.BookOfLore)
        self.assertEqual(self.player.GetMovementAllowance(), 5)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumMerchants(), self.startingNumMerchants + 3)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders)
        self.assertFalse(self.player.UpgradeSkill(SkillTypes.BookOfLore)[0])

    def testMoneySkill(self):
        self.assertEqual(self.player._GetSkill(SkillTypes.Money).GetValue(), 3)
        self.player.UpgradeSkill(SkillTypes.Money)
        self.assertEqual(self.player._GetSkill(SkillTypes.Money).GetValue(), 5)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 1)
        self.player.UpgradeSkill(SkillTypes.Money)
        self.assertEqual(self.player._GetSkill(SkillTypes.Money).GetValue(), 7)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 2)
        self.player.UpgradeSkill(SkillTypes.Money)
        self.assertEqual(self.player._GetSkill(SkillTypes.Money).GetValue(), sys.maxsize)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumTraders(), self.startingNumTraders + 3)
        self.assertEqual(self.player.personalSupplyResourcePool.GetNumMerchants(), self.startingNumMerchants)
        self.assertFalse(self.player.UpgradeSkill(SkillTypes.Money)[0])

    def testTwoSkillsIndependentUpgrade(self):
        self.player.UpgradeSkill(SkillTypes.TownKey)
        self.assertEqual(self.player.GetTownKeyValue(), 2)
        self.assertTrue(self.player.HasPrivilegeLevel(1))

    def testPlayerIndependentSkillUpgrade(self):
        self.player2.UpgradeSkill(SkillTypes.BookOfLore)

        self.assertNotEqual(
            self.player._GetSkill(SkillTypes.BookOfLore).GetValue(),
            self.player2._GetSkill(SkillTypes.BookOfLore).GetValue())

    def testSkillMastery(self):
        self.player.UpgradeSkill(SkillTypes.Privilege)
        self.player.UpgradeSkill(SkillTypes.Privilege)
        self.player.UpgradeSkill(SkillTypes.Privilege)
        self.assertEqual(self.player.GetNumMasteredSkills(), 1)
        self.assertEqual(self.player2.GetNumMasteredSkills(), 0)

    def CreatePlayer(self, playerName, playerColor):
        return Player(playerName, playerName, playerColor)

def TheTestSuite():
    return unittest.defaultTestLoader.loadTestsFromTestCase(PlayerSkillsTestCase)

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(TheTestSuite())

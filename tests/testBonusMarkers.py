if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from hansa.GameManager import GameManager
from hansa.Skill import SkillTypes
from hansa.Resource import Resource, ResourceType
from hansa.Action import PlaceResourceAction, ResupplyAction, PlaceBonusMarkersAction, UseBonusMarkerAction
from hansa.BonusMarker import ExtraActions3BonusMarker, ExtraActions4BonusMarker, IncreaseSkillBonusMarker, RemoveResources3BonusMarker, SwapOfficesBonusMarker
from hansa.BonusMarker import BonusMarker

import unittest

class BonusMarkersTestCase(unittest.TestCase):
    def setUp(self):
        self.gameManager = GameManager(True)
        id = self.gameManager.CreateGame("TestGame", "Mr Test")
        self.game = self.gameManager.GetGameByID(id)
        self.game.AddPlayer("Player One")
        self.game.AddPlayer("Player Two")
        self.game.Start()

    def tearDown(self):
        self.gameManager.Shutdown()

    def testUseBonusMarkerAction_NoBonusMarker(self):
        self.game.currentPlayer.TakeBonusMarker(IncreaseSkillBonusMarker())
        self.assertEqual(self.game.currentPlayer.GetNumBonusMarkers(), 1)

        numActionsBefore = self.game.currentPlayer.numActionsRemaining

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(ExtraActions3BonusMarker()))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, numActionsBefore)
        self.assertIsNotNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(IncreaseSkillBonusMarker()))

    def testUseBonusMarkerAction_DifferentBonusMarkerVariantOfSameType(self):
        self.game.currentPlayer.TakeBonusMarker(ExtraActions3BonusMarker())
        self.assertEqual(self.game.currentPlayer.GetNumBonusMarkers(), 1)

        numActionsBefore = self.game.currentPlayer.numActionsRemaining

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(ExtraActions4BonusMarker()))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, numActionsBefore)
        self.assertIsNotNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(ExtraActions3BonusMarker()))

    def testUseBonusMarkerAction_InvalidBonusMarker(self):
        invalidBonusMarker = BonusMarker("abc123")
        self.game.currentPlayer.TakeBonusMarker(invalidBonusMarker)
        self.assertEqual(self.game.currentPlayer.GetNumBonusMarkers(), 1)

        numActionsBefore = self.game.currentPlayer.numActionsRemaining

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(invalidBonusMarker))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, numActionsBefore)
        self.assertIsNotNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(invalidBonusMarker))

    def testExtraActionsBonusMarkers(self):
        self.game.currentPlayer.TakeBonusMarker(ExtraActions3BonusMarker())
        self.game.currentPlayer.TakeBonusMarker(ExtraActions4BonusMarker())
        self.assertEqual(self.game.currentPlayer.GetNumBonusMarkers(), 2)

        numActionsBefore = self.game.currentPlayer.numActionsRemaining

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(ExtraActions3BonusMarker()))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, numActionsBefore + 3)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(ExtraActions4BonusMarker()))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, numActionsBefore + 3 + 4)
        self.assertIsNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker()))

    def testIncreaseSkillBonusMarker(self):
        self.game.currentPlayer.TakeBonusMarker(IncreaseSkillBonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(IncreaseSkillBonusMarker())
        bonusMarker.PrepareForUse(SkillTypes.TownKey)

        numActionsBefore = self.game.currentPlayer.numActionsRemaining
        skillValueBefore = self.game.currentPlayer._GetSkill(bonusMarker.skillType).GetValue()

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, numActionsBefore)
        self.assertIsNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(IncreaseSkillBonusMarker()))
        self.assertEqual(self.game.currentPlayer._GetSkill(bonusMarker.skillType).GetValue(), skillValueBefore + 1)

    def testIncreaseSkillBonusMarker_InvalidSkill(self):
        self.game.currentPlayer.TakeBonusMarker(IncreaseSkillBonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(IncreaseSkillBonusMarker())
        bonusMarker.PrepareForUse(999)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertIsNotNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(IncreaseSkillBonusMarker()))

    def testRemoveResources3BonusMarker(self):
        self.game.currentPlayer.TakeBonusMarker(RemoveResources3BonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker())

        fieldIDs = ["1-2.1", "1-2.2", "1-2.3"]
        resourceType = ResourceType.Trader

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[1]))

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[2]))

        bonusMarker.PrepareForUse(fieldIDs)

        numActionsBefore = self.game.currentPlayer.numActionsRemaining

        for i, fieldID in enumerate(fieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())
            self.assertEqual(field.GetOccupyingResource().GetOwnerPlayerID(), self.game.currentPlayer.GetID())
            self.assertEqual(field.GetOccupyingResource().GetType(), resourceType)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, numActionsBefore)
        self.assertIsNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker()))

        for i, fieldID in enumerate(fieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertFalse(field.IsOccupied())

    def testRemoveResources3BonusMarker_FewerFields(self):
        self.game.currentPlayer.TakeBonusMarker(RemoveResources3BonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker())

        fieldIDs = ["1-2.1", "1-2.2"]
        resourceType = ResourceType.Trader

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[1]))

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        bonusMarker.PrepareForUse(fieldIDs)

        numActionsBefore = self.game.currentPlayer.numActionsRemaining

        for i, fieldID in enumerate(fieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())
            self.assertEqual(field.GetOccupyingResource().GetOwnerPlayerID(), self.game.currentPlayer.GetID())
            self.assertEqual(field.GetOccupyingResource().GetType(), resourceType)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, numActionsBefore)
        self.assertIsNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker()))

        for i, fieldID in enumerate(fieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertFalse(field.IsOccupied())

    def testRemoveResources3BonusMarker_TooManyFields(self):
        self.game.currentPlayer.TakeBonusMarker(RemoveResources3BonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker())

        fieldIDs = ["1-2.1", "1-2.2", "1-2.3", "2-3.1"]
        resourceType = ResourceType.Trader

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[1]))

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[2]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[3]))

        bonusMarker.PrepareForUse(fieldIDs)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertIsNotNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker()))

        for i, fieldID in enumerate(fieldIDs):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())

    def testRemoveResources3BonusMarker_FieldNotOccupied(self):
        self.game.currentPlayer.TakeBonusMarker(RemoveResources3BonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker())

        fieldIDs = ["1-2.1", "1-2.2", "1-2.3"]
        resourceType = ResourceType.Trader

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[1]))

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        bonusMarker.PrepareForUse(fieldIDs)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertIsNotNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker()))

        for i, fieldID in enumerate([fieldIDs[0], fieldIDs[1]]):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())

    def testRemoveResources3BonusMarker_InvalidFieldID(self):
        self.game.currentPlayer.TakeBonusMarker(RemoveResources3BonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker())

        fieldIDs = ["1-2.1", "1-2.2", "123abc"]
        resourceType = ResourceType.Trader

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[0]))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), PlaceResourceAction(resourceType, fieldIDs[1]))

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))
        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), ResupplyAction(0, 0))

        bonusMarker.PrepareForUse(fieldIDs)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertIsNotNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(RemoveResources3BonusMarker()))

        for i, fieldID in enumerate([fieldIDs[0], fieldIDs[1]]):
            field = self.game.board.GetFieldByID(fieldID)
            self.assertTrue(field.IsOccupied())

    def testSwapOfficesBonusMarker(self):
        playerIDs = [self.game.currentPlayer.GetID()]
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))
        playerIDs.append(self.game.currentPlayer.GetID())

        self.game.currentPlayer.TakeBonusMarker(SwapOfficesBonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(SwapOfficesBonusMarker())
        cityID = "1"

        numActionsBefore = self.game.currentPlayer.numActionsRemaining

        resource1 = Resource(ResourceType.Trader, playerIDs[0])
        resource2 = Resource(ResourceType.Merchant, playerIDs[1])

        self.game.board.GetCityByID(cityID).GetOfficeByIndex(0).Occupy(resource1)
        self.game.board.GetCityByID(cityID).GetOfficeByIndex(1).Occupy(resource2)

        bonusMarker.PrepareForUse(cityID, 0)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertEqual(self.game.currentPlayer.numActionsRemaining, numActionsBefore)
        self.assertIsNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(SwapOfficesBonusMarker()))

        self.assertEqual(self.game.board.GetCityByID(cityID).GetOfficeByIndex(0).occupyingResource.GetType(), resource2.GetType())
        self.assertEqual(self.game.board.GetCityByID(cityID).GetOfficeByIndex(1).occupyingResource.GetType(), resource1.GetType())

        self.assertEqual(self.game.board.GetCityByID(cityID).GetOfficeByIndex(0).GetOccupyingPlayerID(), playerIDs[1])
        self.assertEqual(self.game.board.GetCityByID(cityID).GetOfficeByIndex(1).GetOccupyingPlayerID(), playerIDs[0])

    def testSwapOfficesBonusMarker_EmptyOffice(self):
        playerID = self.game.currentPlayer.GetID()

        self.game.currentPlayer.TakeBonusMarker(SwapOfficesBonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(SwapOfficesBonusMarker())
        cityID = "1"

        resource = Resource(ResourceType.Trader, playerID)
        self.game.board.GetCityByID(cityID).GetOfficeByIndex(0).Occupy(resource)
        bonusMarker.PrepareForUse(cityID, 0)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertIsNotNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(SwapOfficesBonusMarker()))
        self.assertEqual(self.game.board.GetCityByID(cityID).GetOfficeByIndex(0).occupyingResource.GetType(), resource.GetType())
        self.assertEqual(self.game.board.GetCityByID(cityID).GetOfficeByIndex(0).GetOccupyingPlayerID(), playerID)

    def testSwapOfficesBonusMarker_RightmostOffice(self):
        playerID = self.game.currentPlayer.GetID()

        self.game.currentPlayer.TakeBonusMarker(SwapOfficesBonusMarker())
        bonusMarker = self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(SwapOfficesBonusMarker())
        cityID = "1"

        resource = Resource(ResourceType.Trader, playerID)
        self.game.board.GetCityByID(cityID).GetOfficeByIndex(1).Occupy(resource)
        bonusMarker.PrepareForUse(cityID, 1)

        self.game.DoPlayerAction(self.game.currentPlayer.GetID(), UseBonusMarkerAction(bonusMarker))
        self.assertIsNotNone(self.game.currentPlayer.GetUnusedBonusMarkerOfClassType(SwapOfficesBonusMarker()))
        self.assertEqual(self.game.board.GetCityByID(cityID).GetOfficeByIndex(1).occupyingResource.GetType(), resource.GetType())
        self.assertEqual(self.game.board.GetCityByID(cityID).GetOfficeByIndex(1).GetOccupyingPlayerID(), playerID)

    def testPlaceBonusMarkersAction(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)

        targetFieldID = "1-2.1"
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction([targetFieldID]))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 0)
        self.assertTrue(self.game.board.GetRouteByFieldID(targetFieldID).HasBonusMarker())

    def testPlaceBonusMarkersAction_NoneToPlace(self):
        playerID = self.game.currentPlayer.GetID()

        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))

        targetFieldID = "1-2.1"
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction([targetFieldID]))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 0)
        self.assertFalse(self.game.board.GetRouteByFieldID(targetFieldID).HasBonusMarker())

    def testPlaceBonusMarkersAction_WrongPlayer(self):
        playerIDs = [self.game.currentPlayer.GetID()]

        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[0], ResupplyAction(0, 0))

        playerIDs.append(self.game.currentPlayer.GetID())

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerIDs[1], ResupplyAction(0, 0))

        targetFieldID = "1-2.1"
        self.game.DoPlayerAction(playerIDs[0], PlaceBonusMarkersAction([targetFieldID]))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 1)
        self.assertFalse(self.game.board.GetRouteByFieldID(targetFieldID).HasBonusMarker())

    def testPlaceBonusMarkersAction_WrongAction(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))

        targetFieldID = "1-2.1"
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, targetFieldID))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 1)
        self.assertFalse(self.game.board.GetRouteByFieldID(targetFieldID).HasBonusMarker())

    def testPlaceBonusMarkersAction_NotEndOfTurn(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        targetFieldID = "1-2.1"
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction([targetFieldID]))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 1)
        self.assertFalse(self.game.board.GetRouteByFieldID(targetFieldID).HasBonusMarker())

    def testPlaceBonusMarkersAction_RouteAlreadyHasBonusMarker(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))

        targetFieldID = "20-21.1"
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction([targetFieldID]))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 1)

    def testPlaceBonusMarkersAction_RouteIsNotEmpty(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, "1-2.1"))
        self.game.DoPlayerAction(playerID, PlaceResourceAction(ResourceType.Trader, "1-2.2"))

        targetFieldID = "1-2.3"
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction([targetFieldID]))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 1)

    def testPlaceBonusMarkersAction_AllOfficesAreOccupied(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        resource1 = Resource(ResourceType.Trader, playerID)
        resource2 = Resource(ResourceType.Merchant, playerID)

        self.game.board.GetCityByID("1").GetOfficeByIndex(0).Occupy(resource1)
        self.game.board.GetCityByID("1").GetOfficeByIndex(1).Occupy(resource2)
        self.game.board.GetCityByID("2").GetOfficeByIndex(0).Occupy(resource1)
        self.game.board.GetCityByID("2").GetOfficeByIndex(1).Occupy(resource2)

        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))

        targetFieldID = "1-2.3"
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction([targetFieldID]))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 1)

    def testPlaceBonusMarkersAction_Multiple(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()
        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 2)

        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)

        targetFieldIDs = ["1-2.1", "2-3.2"]
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction(targetFieldIDs))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 2)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 0)
        self.assertTrue(self.game.board.GetRouteByFieldID(targetFieldIDs[0]).HasBonusMarker())
        self.assertTrue(self.game.board.GetRouteByFieldID(targetFieldIDs[1]).HasBonusMarker())

    def testPlaceBonusMarkersAction_TooManyFieldsSpecified(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()
        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 2)

        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)

        targetFieldIDs = ["1-2.1", "2-3.2", "3-4.5"]
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction(targetFieldIDs))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 2)
        self.assertFalse(self.game.board.GetRouteByFieldID(targetFieldIDs[0]).HasBonusMarker())
        self.assertFalse(self.game.board.GetRouteByFieldID(targetFieldIDs[1]).HasBonusMarker())

    def testPlaceBonusMarkersAction_NotAllFieldsSpecified(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()
        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 2)

        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)

        targetFieldIDs = ["1-2.1"]
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction(targetFieldIDs))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 2)
        self.assertFalse(self.game.board.GetRouteByFieldID(targetFieldIDs[0]).HasBonusMarker())

    def testPlaceBonusMarkersAction_InvalidRoute(self):
        playerID = self.game.currentPlayer.GetID()

        self.game._AddBonusMarkerToPlaceAtEndOfTurn()

        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))
        self.game.DoPlayerAction(playerID, ResupplyAction(0, 0))

        targetFieldID = "123abc"
        self.game.DoPlayerAction(playerID, PlaceBonusMarkersAction([targetFieldID]))

        self.assertEqual(self.game.GetState().currentPlayer.numActionsRemaining, 0)
        self.assertEqual(len(self.game.GetState().bonusMarkersToPlaceAtEndOfTurn), 1)

def TheTestSuite():
    return unittest.defaultTestLoader.loadTestsFromTestCase(BonusMarkersTestCase)

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(TheTestSuite())
